/*
 * GStreamer
 * Copyright (C) <2005> Julien Moutte <julien@moutte.net>
 *               <2009>,<2010> Stefan Kost <stefan.kost@nokia.com>
 *               <2011> Texas Instruments, Inc. <daniels@collabora.com>
 * Copyright (c) 2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-nvxvimagesink
 *
 * NVXvImageSink is derived from GStreamer's base XvImageSink, with the
 * additional property that it only accepts frames which have been
 * allocated by an external physically-contiguous allocator; it wraps
 * these frames in a new NV buffer, and submits that buffer to the X server
 * using a modified Xv extension, without performing any copying.
 *
 * As we only get a pointer to the buffer storage, we differentiate
 * this from normal video by introducing a new video/x-nvrm-yuv cap.
 *
 * The documentation that follows below describes the standard XvImageSink,
 * and thus may be incorrect.  Caveat emptor.
 *
 * -------------------------------------------------------------------------
 *
 * XvImageSink renders video frames to a drawable (XWindow) on a local display
 * using the XVideo extension. Rendering to a remote display is theorically
 * possible but i doubt that the XVideo extension is actually available when
 * connecting to a remote display. This element can receive a Window ID from the
 * application through the XOverlay interface and will then render video frames
 * in this drawable. If no Window ID was provided by the application, the
 * element will create its own internal window and render into it.
 *
 * <refsect2>
 * <title>Scaling</title>
 * <para>
 * The XVideo extension, when it's available, handles hardware accelerated
 * scaling of video frames. This means that the element will just accept
 * incoming video frames no matter their geometry and will then put them to the
 * drawable scaling them on the fly. Using the #GstNVXvImageSink:force-aspect-ratio
 * property it is possible to enforce scaling with a constant aspect ratio,
 * which means drawing black borders around the video frame.
 * </para>
 * </refsect2>
 * <refsect2>
 * <title>Events</title>
 * <para>
 * XvImageSink creates a thread to handle events coming from the drawable. There
 * are several kind of events that can be grouped in 2 big categories: input
 * events and window state related events. Input events will be translated to
 * navigation events and pushed upstream for other elements to react on them.
 * This includes events such as pointer moves, key press/release, clicks etc...
 * Other events are used to handle the drawable appearance even when the data
 * is not flowing (GST_STATE_PAUSED). That means that even when the element is
 * paused, it will receive expose events from the drawable and draw the latest
 * frame with correct borders/aspect-ratio.
 * </para>
 * </refsect2>
 * <refsect2>
 * <title>Pixel aspect ratio</title>
 * <para>
 * When changing state to GST_STATE_READY, XvImageSink will open a connection to
 * the display specified in the #GstNVXvImageSink:display property or the
 * default display if nothing specified. Once this connection is open it will
 * inspect the display configuration including the physical display geometry and
 * then calculate the pixel aspect ratio. When receiving video frames with a
 * different pixel aspect ratio, XvImageSink will use hardware scaling to
 * display the video frames correctly on display's pixel aspect ratio.
 * Sometimes the calculated pixel aspect ratio can be wrong, it is
 * then possible to enforce a specific pixel aspect ratio using the
 * #GstNVXvImageSink:pixel-aspect-ratio property.
 * </para>
 * </refsect2>
 * <refsect2>
 * <title>Examples</title>
 * |[
 * gst-launch -v videotestsrc ! nvxvimagesink
 * ]| A pipeline to test hardware scaling.
 * When the test video signal appears you can resize the window and see that
 * video frames are scaled through hardware (no extra CPU cost).
 * |[
 * gst-launch -v videotestsrc ! nvxvimagesink force-aspect-ratio=true
 * ]| Same pipeline with #GstNVXvImageSink:force-aspect-ratio property set to true
 * You can observe the borders drawn around the scaled image respecting aspect
 * ratio.
 * |[
 * gst-launch -v videotestsrc ! navigationtest ! nvxvimagesink
 * ]| A pipeline to test navigation events.
 * While moving the mouse pointer over the test signal you will see a black box
 * following the mouse pointer. If you press the mouse button somewhere on the
 * video and release it somewhere else a green box will appear where you pressed
 * the button and a red one where you released it. (The navigationtest element
 * is part of gst-plugins-good.) You can observe here that even if the images
 * are scaled through hardware the pointer coordinates are converted back to the
 * original video frame geometry so that the box can be drawn to the correct
 * position. This also handles borders correctly, limiting coordinates to the
 * image area
 * |[
 * gst-launch -v videotestsrc ! video/x-raw-yuv, pixel-aspect-ratio=(fraction)4/3 ! nvxvimagesink
 * ]| This is faking a 4/3 pixel aspect ratio caps on video frames produced by
 * videotestsrc, in most cases the pixel aspect ratio of the display will be
 * 1/1. This means that XvImageSink will have to do the scaling to convert
 * incoming frames to a size that will match the display pixel aspect ratio
 * (from 320x240 to 320x180 in this case). Note that you might have to escape
 * some characters for your shell like '\(fraction\)'.
 * </refsect2>
 */

/* for developers: there are two useful tools : xvinfo and xvattr */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/types.h>

#include "nvmap_ioctl.h"
#include "tegra_video.h"

#include <X11/XKBlib.h>

/* Our interfaces */
#include <gst/interfaces/navigation.h>
#include <gst/interfaces/xoverlay.h>
#include <gst/interfaces/propertyprobe.h>
/* Helper functions */
#include <gst/video/video.h>

/* Object header */
#include "nvxvimagesink.h"

/* Debugging category */
#include <gst/gstinfo.h>
GST_DEBUG_CATEGORY_STATIC (gst_debug_nvxvimagesink);
#define GST_CAT_DEFAULT gst_debug_nvxvimagesink
GST_DEBUG_CATEGORY_STATIC (GST_CAT_PERFORMANCE);

typedef struct
{
  unsigned long flags;
  unsigned long functions;
  unsigned long decorations;
  long input_mode;
  unsigned long status;
}
MotifWmHints, MwmHints;

#define MWM_HINTS_DECORATIONS   (1L << 1)

static void gst_nvxvimagesink_reset (GstNVXvImageSink * nvxvimagesink);

static void gst_nvxvimagesink_xwindow_update_geometry (GstNVXvImageSink *
    nvxvimagesink);
static gint gst_nvxvimagesink_get_format_from_caps (GstNVXvImageSink *
    nvxvimagesink, GstCaps * caps);
static void gst_nvxvimagesink_expose (GstXOverlay * overlay);
static gboolean
gst_nvxvimagesink_get_nvmap_fd (GstNVXvImageSink * nvxvimagesink);

static GType gst_nvxvimage_buffer_get_type (void);

/* Default template - initiated with class struct to allow gst-register to work
   without X running */
static GstStaticPadTemplate gst_nvxvimagesink_sink_template_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-nvrm-yuv, "
        "framerate = (fraction) [ 0, MAX ], "
        "width = (int) [ 1, MAX ], "
        "height = (int) [ 1, MAX ], " "rowstride = (int) [ 1, MAX ]")
    );

enum
{
  PROP_0,
  PROP_DISPLAY,
  PROP_SYNCHRONOUS,
  PROP_PIXEL_ASPECT_RATIO,
  PROP_FORCE_ASPECT_RATIO,
  PROP_FORCE_FULLSCREEN,
  PROP_HANDLE_EVENTS,
  PROP_DEVICE,
  PROP_DEVICE_NAME,
  PROP_HANDLE_EXPOSE,
  PROP_SHARED_SURFACE,
  PROP_SYNC_OPTIONS,
  PROP_SYNC_TO_VBLANK,
  PROP_DRAW_BORDERS,
  PROP_WINDOW_WIDTH,
  PROP_WINDOW_HEIGHT
};

static GstVideoSinkClass *parent_class = NULL;

/* ============================================================= */
/*                                                               */
/*                       Private Methods                         */
/*                                                               */
/* ============================================================= */

/* xvimage buffers */

#define GST_TYPE_NVXVIMAGE_BUFFER (gst_nvxvimage_buffer_get_type())

/* X11 stuff */

static gboolean error_caught = FALSE;

/* This function destroys a xvimage handling XShm */
static void
gst_nvxvimagesink_image_destroy (GstNVXvImageSink * nvxvimagesink)
{
  int todo = 0;                 /* TODO support multiple images */

  if (G_UNLIKELY (nvxvimagesink == NULL))
    goto no_sink;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  if (G_UNLIKELY (nvxvimagesink->xvimages[todo].xvimage == NULL))
    goto no_image;

  GST_OBJECT_LOCK (nvxvimagesink);

  /* We might have some buffers destroyed after changing state to NULL */
  if (nvxvimagesink->xcontext == NULL) {
    GST_DEBUG_OBJECT (nvxvimagesink, "Destroying XvImage after Xcontext");
    /* Need to free the shared memory segment even if the x context
     * was already cleaned up */
    if (nvxvimagesink->xvimages[todo].SHMInfo.shmaddr != ((void *) -1)) {
      shmdt (nvxvimagesink->xvimages[todo].SHMInfo.shmaddr);
    }
    XFree (nvxvimagesink->xvimages[todo].xvimage);
    goto beach;
  }

  g_mutex_lock (nvxvimagesink->x_lock);

  if (nvxvimagesink->xvimages[todo].SHMInfo.shmaddr != ((void *) -1)) {
    GST_DEBUG_OBJECT (nvxvimagesink, "XServer ShmDetaching from 0x%x id 0x%lx",
        nvxvimagesink->xvimages[todo].SHMInfo.shmid,
        nvxvimagesink->xvimages[todo].SHMInfo.shmseg);
    XShmDetach (nvxvimagesink->xcontext->disp,
        &nvxvimagesink->xvimages[todo].SHMInfo);
    XSync (nvxvimagesink->xcontext->disp, FALSE);

    shmdt (nvxvimagesink->xvimages[todo].SHMInfo.shmaddr);
  }
  XFree (nvxvimagesink->xvimages[todo].xvimage);

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);

beach:
  GST_OBJECT_UNLOCK (nvxvimagesink);
  nvxvimagesink->xvimages[todo].xvimage = NULL;
  return;

no_image:
  {
    GST_WARNING ("no image found");
    return;
  }
no_sink:
  {
    GST_WARNING ("no sink found");
    return;
  }
}

static int
gst_nvxvimagesink_handle_xerror (Display * display, XErrorEvent * xevent)
{
  char error_msg[1024];

  XGetErrorText (display, xevent->error_code, error_msg, 1024);
  GST_DEBUG ("xvimagesink triggered an XError. error: %s", error_msg);
  error_caught = TRUE;
  return 0;
}

/* This function checks that it is actually really possible to create an image
   using XShm */
static gboolean
gst_xvimagesink_check_xshm_calls (GstXContext * xcontext)
{
  XvImage *xvimage;
  XShmSegmentInfo SHMInfo;
  gint size;
  int (*handler) (Display *, XErrorEvent *);
  gboolean result = FALSE;
  gboolean did_attach = FALSE;

  g_return_val_if_fail (xcontext != NULL, FALSE);

  /* Sync to ensure any older errors are already processed */
  XSync (xcontext->disp, FALSE);

  /* Set defaults so we don't free these later unnecessarily */
  SHMInfo.shmaddr = ((void *) -1);
  SHMInfo.shmid = -1;

  /* Setting an error handler to catch failure */
  error_caught = FALSE;
  handler = XSetErrorHandler (gst_nvxvimagesink_handle_xerror);

  /* Trying to create a 1x1 picture */
  GST_DEBUG ("XvShmCreateImage of 1x1");
  xvimage = XvShmCreateImage (xcontext->disp, xcontext->xv_port_id,
      xcontext->im_format, NULL, 1, 1, &SHMInfo);

  /* Might cause an error, sync to ensure it is noticed */
  XSync (xcontext->disp, FALSE);
  if (!xvimage || error_caught) {
    GST_WARNING ("could not XvShmCreateImage a 1x1 image");
    goto beach;
  }
  size = xvimage->data_size;

  SHMInfo.shmid = shmget (IPC_PRIVATE, size, IPC_CREAT | 0777);
  if (SHMInfo.shmid == -1) {
    GST_WARNING ("could not get shared memory of %d bytes", size);
    goto beach;
  }

  SHMInfo.shmaddr = shmat (SHMInfo.shmid, NULL, 0);
  if (SHMInfo.shmaddr == ((void *) -1)) {
    GST_WARNING ("Failed to shmat: %s", g_strerror (errno));
    /* Clean up the shared memory segment */
    shmctl (SHMInfo.shmid, IPC_RMID, NULL);
    goto beach;
  }

  xvimage->data = SHMInfo.shmaddr;
  SHMInfo.readOnly = FALSE;

  if (XShmAttach (xcontext->disp, &SHMInfo) == 0) {
    GST_WARNING ("Failed to XShmAttach");
    /* Clean up the shared memory segment */
    shmctl (SHMInfo.shmid, IPC_RMID, NULL);
    goto beach;
  }

  /* Sync to ensure we see any errors we caused */
  XSync (xcontext->disp, FALSE);

  /* Delete the shared memory segment as soon as everyone is attached.
   * This way, it will be deleted as soon as we detach later, and not
   * leaked if we crash. */
  shmctl (SHMInfo.shmid, IPC_RMID, NULL);

  if (!error_caught) {
    GST_DEBUG ("XServer ShmAttached to 0x%x, id 0x%lx", SHMInfo.shmid,
        SHMInfo.shmseg);

    did_attach = TRUE;
    /* store whether we succeeded in result */
    result = TRUE;
  } else {
    GST_WARNING ("MIT-SHM extension check failed at XShmAttach. "
        "Not using shared memory.");
  }

beach:
  /* Sync to ensure we swallow any errors we caused and reset error_caught */
  XSync (xcontext->disp, FALSE);

  error_caught = FALSE;
  XSetErrorHandler (handler);

  if (did_attach) {
    GST_DEBUG ("XServer ShmDetaching from 0x%x id 0x%lx",
        SHMInfo.shmid, SHMInfo.shmseg);
    XShmDetach (xcontext->disp, &SHMInfo);
    XSync (xcontext->disp, FALSE);
  }
  if (SHMInfo.shmaddr != ((void *) -1))
    shmdt (SHMInfo.shmaddr);
  if (xvimage)
    XFree (xvimage);
  return result;
}

static void
gst_nvxvimagesink_buffer_free (GstNVXvImageBuffer * buf_nv)
{
  GST_DEBUG_OBJECT (buf_nv->nvxvimagesink, "returning buffer 0x%x\n",
      buf_nv->id);

  if (buf_nv->buf_gst) {
    gst_buffer_unref (buf_nv->buf_gst);
  }
  gst_object_unref (buf_nv->nvxvimagesink);
  gst_mini_object_unref (GST_MINI_OBJECT_CAST (buf_nv));
}

/* Create a new NV buffer wrapping the storage of an existing GstBuffer. */
static GstNVXvImageBuffer *
gst_nvxvimagesink_buffer_wrap (GstNVXvImageSink * nvxvimagesink,
    GstBuffer * buf_gst)
{
  GstNVXvImageBuffer *buf_nv = NULL;

  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), NULL);

  buf_nv =
      (GstNVXvImageBuffer *) gst_mini_object_new (GST_TYPE_NVXVIMAGE_BUFFER);
  if (!buf_nv) {
    return NULL;
  }
  buf_nv->nvxvimagesink = gst_object_ref (nvxvimagesink);
  buf_nv->buf_gst = gst_buffer_ref (buf_gst);

  return buf_nv;
}

/* We are called with the x_lock taken */
static void
gst_nvxvimagesink_xwindow_draw_borders (GstNVXvImageSink * nvxvimagesink,
    GstXWindow * xwindow, GstVideoRectangle rect)
{
  gint t1, t2;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));
  g_return_if_fail (xwindow != NULL);

  XSetForeground (nvxvimagesink->xcontext->disp, xwindow->gc,
      nvxvimagesink->xcontext->black);

  /* Left border */
  if (rect.x > nvxvimagesink->render_rect.x) {
    XFillRectangle (nvxvimagesink->xcontext->disp, xwindow->win, xwindow->gc,
        nvxvimagesink->render_rect.x, nvxvimagesink->render_rect.y,
        rect.x - nvxvimagesink->render_rect.x, nvxvimagesink->render_rect.h);
  }

  /* Right border */
  t1 = rect.x + rect.w;
  t2 = nvxvimagesink->render_rect.x + nvxvimagesink->render_rect.w;
  if (t1 < t2) {
    XFillRectangle (nvxvimagesink->xcontext->disp, xwindow->win, xwindow->gc,
        t1, nvxvimagesink->render_rect.y, t2 - t1,
        nvxvimagesink->render_rect.h);
  }

  /* Top border */
  if (rect.y > nvxvimagesink->render_rect.y) {
    XFillRectangle (nvxvimagesink->xcontext->disp, xwindow->win, xwindow->gc,
        nvxvimagesink->render_rect.x, nvxvimagesink->render_rect.y,
        nvxvimagesink->render_rect.w, rect.y - nvxvimagesink->render_rect.y);
  }

  /* Bottom border */
  t1 = rect.y + rect.h;
  t2 = nvxvimagesink->render_rect.y + nvxvimagesink->render_rect.h;
  if (t1 < t2) {
    XFillRectangle (nvxvimagesink->xcontext->disp, xwindow->win, xwindow->gc,
        nvxvimagesink->render_rect.x, t1, nvxvimagesink->render_rect.w,
        t2 - t1);
  }
}

/* This function puts a GstXvImage on a GstNVXvImageSink's window. Returns FALSE
 * if no window was available  */
static gboolean
gst_nvxvimagesink_nvxvimage_put (GstNVXvImageSink * nvxvimagesink,
    GstNVXvImageBuffer * buf_nv)
{
  GstVideoRectangle result;
  gboolean draw_border = FALSE;
  int i, todo = 0;              /* TODO support multiple images */
  tegraXVSurfaceDescriptor nvSharedSurface;
  struct nvmap_create_handle op = { {0} };

  /* We take the flow_lock. If expose is in there we don't want to run
     concurrently from the data flow thread */
  g_mutex_lock (nvxvimagesink->flow_lock);

  if (G_UNLIKELY (nvxvimagesink->xwindow == NULL)) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    return FALSE;
  }

  /* Draw borders when displaying the first frame. After this
     draw borders only on expose event or after a size change. */
  if (nvxvimagesink->redraw_border) {
    draw_border = TRUE;
  }

  /*TODO Expose sends a NULL image, we should take the last rendered frame */
  if (!buf_nv) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    return TRUE;
  }

  if (nvxvimagesink->keep_aspect) {
    GstVideoRectangle src, dst;

    /* We use the calculated geometry from _setcaps as a source to respect
       source and screen pixel aspect ratios. */
    src.w = GST_VIDEO_SINK_WIDTH (nvxvimagesink);
    src.h = GST_VIDEO_SINK_HEIGHT (nvxvimagesink);
    dst.w = nvxvimagesink->render_rect.w;
    dst.h = nvxvimagesink->render_rect.h;

    gst_video_sink_center_rect (src, dst, &result, TRUE);
    result.x += nvxvimagesink->render_rect.x;
    result.y += nvxvimagesink->render_rect.y;
  } else {
    memcpy (&result, &nvxvimagesink->render_rect, sizeof (GstVideoRectangle));
  }

  g_mutex_lock (nvxvimagesink->x_lock);

  if (draw_border && nvxvimagesink->draw_borders) {
    gst_nvxvimagesink_xwindow_draw_borders (nvxvimagesink,
        nvxvimagesink->xwindow, result);
    nvxvimagesink->redraw_border = FALSE;
  }

  memcpy ((unsigned char *) nvSharedSurface.rmsurfs,
      (unsigned char *) GST_BUFFER_DATA (buf_nv->buf_gst),
      GST_BUFFER_SIZE (buf_nv->buf_gst));

  nvSharedSurface.surfCount =
      GST_BUFFER_SIZE (buf_nv->buf_gst) / sizeof (tegraRMSurface);
  if (nvSharedSurface.surfCount > TEGRAXVSURFACEDESCRIPTOR_MAX_SURFACES)
    goto beach;

  for (i = 0; i < nvSharedSurface.surfCount; i++) {
    op.handle = nvSharedSurface.rmsurfs[i].hMem;
    op.id = 0;
    if (ioctl (nvxvimagesink->nvmap_fd, (int) NVMAP_IOC_GET_ID, &op))
      goto beach;
    nvSharedSurface.memIds[i] = op.id;
  }
  nvSharedSurface.size = sizeof (tegraXVSurfaceDescriptor);
  memcpy ((unsigned char *) nvxvimagesink->xvimages[todo].xvimage->data,
      (unsigned char *) (&nvSharedSurface), sizeof (tegraXVSurfaceDescriptor));

  /* We scale to the window's geometry */
  XvShmPutImage (nvxvimagesink->xcontext->disp,
      nvxvimagesink->xcontext->xv_port_id,
      nvxvimagesink->xwindow->win,
      nvxvimagesink->xwindow->gc,
      nvxvimagesink->xvimages[todo].xvimage, nvxvimagesink->disp_x,
      nvxvimagesink->disp_y, nvxvimagesink->disp_width,
      nvxvimagesink->disp_height, result.x, result.y, result.w, result.h,
      FALSE);

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);
  g_mutex_unlock (nvxvimagesink->flow_lock);
  return TRUE;

beach:
  g_mutex_unlock (nvxvimagesink->x_lock);
  g_mutex_unlock (nvxvimagesink->flow_lock);
  return FALSE;
}

static gboolean
gst_nvxvimagesink_xwindow_decorate (GstNVXvImageSink * nvxvimagesink,
    GstXWindow * window)
{
  Atom hints_atom = None;
  MotifWmHints *hints;

  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), FALSE);
  g_return_val_if_fail (window != NULL, FALSE);

  g_mutex_lock (nvxvimagesink->x_lock);

  hints_atom = XInternAtom (nvxvimagesink->xcontext->disp, "_MOTIF_WM_HINTS",
      True);
  if (hints_atom == None) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    return FALSE;
  }

  hints = g_malloc0 (sizeof (MotifWmHints));

  hints->flags |= MWM_HINTS_DECORATIONS;
  hints->decorations = 1 << 0;

  XChangeProperty (nvxvimagesink->xcontext->disp, window->win,
      hints_atom, hints_atom, 32, PropModeReplace,
      (guchar *) hints, sizeof (MotifWmHints) / sizeof (long));

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);

  g_free (hints);

  return TRUE;
}

static void
gst_nvxvimagesink_xwindow_set_title (GstNVXvImageSink * nvxvimagesink,
    GstXWindow * xwindow, const gchar * media_title)
{
  if (media_title) {
    g_free (nvxvimagesink->media_title);
    nvxvimagesink->media_title = g_strdup (media_title);
  }
  if (xwindow) {
    /* we have a window */
    if (xwindow->internal) {
      XTextProperty xproperty;
      const gchar *app_name;
      const gchar *title = NULL;
      gchar *title_mem = NULL;

      /* set application name as a title */
      app_name = g_get_application_name ();

      if (app_name && nvxvimagesink->media_title) {
        title = title_mem = g_strconcat (nvxvimagesink->media_title, " : ",
            app_name, NULL);
      } else if (app_name) {
        title = app_name;
      } else if (nvxvimagesink->media_title) {
        title = nvxvimagesink->media_title;
      }

      if (title) {
        if ((XStringListToTextProperty (((char **) &title), 1,
                    &xproperty)) != 0) {
          XSetWMName (nvxvimagesink->xcontext->disp, xwindow->win, &xproperty);
          XFree (xproperty.value);
        }

        g_free (title_mem);
      }
    }
  }
}

/* This function handles a GstXWindow creation
 * The width and height are the actual pixel size on the display */
static GstXWindow *
gst_nvxvimagesink_xwindow_new (GstNVXvImageSink * nvxvimagesink,
    gint width, gint height)
{
  GstXWindow *xwindow = NULL;
  XGCValues values;

  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), NULL);

  xwindow = g_new0 (GstXWindow, 1);

  nvxvimagesink->render_rect.x = nvxvimagesink->render_rect.y = 0;
  nvxvimagesink->render_rect.w = width;
  nvxvimagesink->render_rect.h = height;

  xwindow->width = width;
  xwindow->height = height;
  xwindow->internal = TRUE;

  g_mutex_lock (nvxvimagesink->x_lock);

  xwindow->win = XCreateSimpleWindow (nvxvimagesink->xcontext->disp,
      nvxvimagesink->xcontext->root,
      0, 0, width, height, 0, 0, nvxvimagesink->xcontext->black);

  /* We have to do that to prevent X from redrawing the background on
   * ConfigureNotify. This takes away flickering of video when resizing. */
  XSetWindowBackgroundPixmap (nvxvimagesink->xcontext->disp, xwindow->win,
      None);

  /* set application name as a title */
  gst_nvxvimagesink_xwindow_set_title (nvxvimagesink, xwindow, NULL);

  if (nvxvimagesink->handle_events) {
    Atom wm_delete;

    XSelectInput (nvxvimagesink->xcontext->disp, xwindow->win, ExposureMask |
        StructureNotifyMask | PointerMotionMask | KeyPressMask |
        KeyReleaseMask | ButtonPressMask | ButtonReleaseMask);

    /* Tell the window manager we'd like delete client messages instead of
     * being killed */
    wm_delete = XInternAtom (nvxvimagesink->xcontext->disp,
        "WM_DELETE_WINDOW", True);
    if (wm_delete != None) {
      (void) XSetWMProtocols (nvxvimagesink->xcontext->disp, xwindow->win,
          &wm_delete, 1);
    }
  }

  xwindow->gc = XCreateGC (nvxvimagesink->xcontext->disp,
      xwindow->win, 0, &values);

  XMapRaised (nvxvimagesink->xcontext->disp, xwindow->win);

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);

  gst_nvxvimagesink_xwindow_decorate (nvxvimagesink, xwindow);

  gst_x_overlay_got_xwindow_id (GST_X_OVERLAY (nvxvimagesink), xwindow->win);

  return xwindow;
}

/* This function destroys a GstXWindow */
static void
gst_nvxvimagesink_xwindow_destroy (GstNVXvImageSink * nvxvimagesink,
    GstXWindow * xwindow)
{
  g_return_if_fail (xwindow != NULL);
  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  g_mutex_lock (nvxvimagesink->x_lock);

  /* If we did not create that window we just free the GC and let it live */
  if (xwindow->internal)
    XDestroyWindow (nvxvimagesink->xcontext->disp, xwindow->win);
  else
    XSelectInput (nvxvimagesink->xcontext->disp, xwindow->win, 0);

  XFreeGC (nvxvimagesink->xcontext->disp, xwindow->gc);

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);

  g_free (xwindow);
}

static void
gst_nvxvimagesink_xwindow_update_geometry (GstNVXvImageSink * nvxvimagesink)
{
  XWindowAttributes attr;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  /* Update the window geometry */
  g_mutex_lock (nvxvimagesink->x_lock);
  if (G_UNLIKELY (nvxvimagesink->xwindow == NULL)) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    return;
  }

  XGetWindowAttributes (nvxvimagesink->xcontext->disp,
      nvxvimagesink->xwindow->win, &attr);

  nvxvimagesink->xwindow->width = attr.width;
  nvxvimagesink->xwindow->height = attr.height;

  if (!nvxvimagesink->have_render_rect) {
    nvxvimagesink->render_rect.x = nvxvimagesink->render_rect.y = 0;
    nvxvimagesink->render_rect.w = attr.width;
    nvxvimagesink->render_rect.h = attr.height;
  }

  g_mutex_unlock (nvxvimagesink->x_lock);
}

static void
gst_nvxvimagesink_xwindow_clear (GstNVXvImageSink * nvxvimagesink,
    GstXWindow * xwindow)
{
  g_return_if_fail (xwindow != NULL);
  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  g_mutex_lock (nvxvimagesink->x_lock);

  XvStopVideo (nvxvimagesink->xcontext->disp,
      nvxvimagesink->xcontext->xv_port_id, xwindow->win);

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  g_mutex_unlock (nvxvimagesink->x_lock);
}

/* This function handles XEvents that might be in the queue. It generates
   GstEvent that will be sent upstream in the pipeline to handle interactivity
   and navigation. It will also listen for configure events on the window to
   trigger caps renegotiation so on the fly software scaling can work. */
static void
gst_nvxvimagesink_handle_xevents (GstNVXvImageSink * nvxvimagesink)
{
  XEvent e;
  guint pointer_x = 0, pointer_y = 0;
  gboolean pointer_moved = FALSE;
  gboolean exposed = FALSE, configured = FALSE;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  /* Handle Interaction, produces navigation events */

  /* We get all pointer motion events, only the last position is
     interesting. */
  g_mutex_lock (nvxvimagesink->flow_lock);
  g_mutex_lock (nvxvimagesink->x_lock);
  while (XCheckWindowEvent (nvxvimagesink->xcontext->disp,
          nvxvimagesink->xwindow->win, PointerMotionMask, &e)) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_mutex_unlock (nvxvimagesink->flow_lock);

    switch (e.type) {
      case MotionNotify:
        pointer_x = e.xmotion.x;
        pointer_y = e.xmotion.y;
        pointer_moved = TRUE;
        break;
      default:
        break;
    }
    g_mutex_lock (nvxvimagesink->flow_lock);
    g_mutex_lock (nvxvimagesink->x_lock);
  }
  if (pointer_moved) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_mutex_unlock (nvxvimagesink->flow_lock);

    GST_DEBUG ("xvimagesink pointer moved over window at %d,%d",
        pointer_x, pointer_y);
    gst_navigation_send_mouse_event (GST_NAVIGATION (nvxvimagesink),
        "mouse-move", 0, e.xbutton.x, e.xbutton.y);

    g_mutex_lock (nvxvimagesink->flow_lock);
    g_mutex_lock (nvxvimagesink->x_lock);
  }

  /* We get all events on our window to throw them upstream */
  while (XCheckWindowEvent (nvxvimagesink->xcontext->disp,
          nvxvimagesink->xwindow->win,
          KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask,
          &e)) {
    KeySym keysym;

    /* We lock only for the X function call */
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_mutex_unlock (nvxvimagesink->flow_lock);

    switch (e.type) {
      case ButtonPress:
        /* Mouse button pressed over our window. We send upstream
           events for interactivity/navigation */
        GST_DEBUG ("xvimagesink button %d pressed over window at %d,%d",
            e.xbutton.button, e.xbutton.x, e.xbutton.y);
        gst_navigation_send_mouse_event (GST_NAVIGATION (nvxvimagesink),
            "mouse-button-press", e.xbutton.button, e.xbutton.x, e.xbutton.y);
        break;
      case ButtonRelease:
        /* Mouse button released over our window. We send upstream
           events for interactivity/navigation */
        GST_DEBUG ("xvimagesink button %d released over window at %d,%d",
            e.xbutton.button, e.xbutton.x, e.xbutton.y);
        gst_navigation_send_mouse_event (GST_NAVIGATION (nvxvimagesink),
            "mouse-button-release", e.xbutton.button, e.xbutton.x, e.xbutton.y);
        break;
      case KeyPress:
      case KeyRelease:
        /* Key pressed/released over our window. We send upstream
           events for interactivity/navigation */
        GST_DEBUG ("xvimagesink key %d pressed over window at %d,%d",
            e.xkey.keycode, e.xkey.x, e.xkey.y);
        g_mutex_lock (nvxvimagesink->x_lock);
        keysym = XkbKeycodeToKeysym (nvxvimagesink->xcontext->disp,
            e.xkey.keycode, 0, 0);
        g_mutex_unlock (nvxvimagesink->x_lock);
        if (keysym != NoSymbol) {
          char *key_str = NULL;

          g_mutex_lock (nvxvimagesink->x_lock);
          key_str = XKeysymToString (keysym);
          g_mutex_unlock (nvxvimagesink->x_lock);
          gst_navigation_send_key_event (GST_NAVIGATION (nvxvimagesink),
              e.type == KeyPress ? "key-press" : "key-release", key_str);
        } else {
          gst_navigation_send_key_event (GST_NAVIGATION (nvxvimagesink),
              e.type == KeyPress ? "key-press" : "key-release", "unknown");
        }
        break;
      default:
        GST_DEBUG ("xvimagesink unhandled X event (%d)", e.type);
    }
    g_mutex_lock (nvxvimagesink->flow_lock);
    g_mutex_lock (nvxvimagesink->x_lock);
  }

  /* Handle Expose */
  while (XCheckWindowEvent (nvxvimagesink->xcontext->disp,
          nvxvimagesink->xwindow->win, ExposureMask | StructureNotifyMask,
          &e)) {
    switch (e.type) {
      case Expose:
        exposed = TRUE;
        break;
      case ConfigureNotify:
        g_mutex_unlock (nvxvimagesink->x_lock);
        gst_nvxvimagesink_xwindow_update_geometry (nvxvimagesink);
        g_mutex_lock (nvxvimagesink->x_lock);
        configured = TRUE;
        break;
      default:
        break;
    }
  }

  if (nvxvimagesink->handle_expose && (exposed || configured)) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_mutex_unlock (nvxvimagesink->flow_lock);

    /* TODO Ideally it should redraw current frame */
    nvxvimagesink->redraw_border = TRUE;
    gst_nvxvimagesink_expose (GST_X_OVERLAY (nvxvimagesink));

    g_mutex_lock (nvxvimagesink->flow_lock);
    g_mutex_lock (nvxvimagesink->x_lock);
  }

  /* Handle Display events */
  while (XPending (nvxvimagesink->xcontext->disp)) {
    XNextEvent (nvxvimagesink->xcontext->disp, &e);

    switch (e.type) {
      case ClientMessage:{
        Atom wm_delete;

        wm_delete = XInternAtom (nvxvimagesink->xcontext->disp,
            "WM_DELETE_WINDOW", True);
        if (wm_delete != None && wm_delete == (Atom) e.xclient.data.l[0]) {
          /* Handle window deletion by posting an error on the bus */
          GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, NOT_FOUND,
              ("Output window was closed"), (NULL));

          g_mutex_unlock (nvxvimagesink->x_lock);
          gst_nvxvimagesink_xwindow_destroy (nvxvimagesink,
              nvxvimagesink->xwindow);
          nvxvimagesink->xwindow = NULL;
          g_mutex_lock (nvxvimagesink->x_lock);
        }
        break;
      }
      default:
        break;
    }
  }

  g_mutex_unlock (nvxvimagesink->x_lock);
  g_mutex_unlock (nvxvimagesink->flow_lock);
}

static void
gst_lookup_xv_port_from_adaptor (GstXContext * xcontext,
    XvAdaptorInfo * adaptors, int adaptor_no)
{
  gint j;
  gint res;

  /* Do we support XvImageMask ? */
  if (!(adaptors[adaptor_no].type & XvImageMask)) {
    GST_DEBUG ("XV Adaptor %s has no support for XvImageMask",
        adaptors[adaptor_no].name);
    return;
  }

  /* We found such an adaptor, looking for an available port */
  for (j = 0; j < adaptors[adaptor_no].num_ports && !xcontext->xv_port_id; j++) {
    /* We try to grab the port */
    res = XvGrabPort (xcontext->disp, adaptors[adaptor_no].base_id + j, 0);
    if (Success == res) {
      xcontext->xv_port_id = adaptors[adaptor_no].base_id + j;
      GST_DEBUG ("XV Adaptor %s with %ld ports", adaptors[adaptor_no].name,
          adaptors[adaptor_no].num_ports);
    } else {
      GST_DEBUG ("GrabPort %d for XV Adaptor %s failed: %d", j,
          adaptors[adaptor_no].name, res);
    }
  }
}

/* This function generates a caps with all supported format by the first
   Xv grabable port we find. We store each one of the supported formats in a
   format list and append the format to a newly created caps that we return
   If this function does not return NULL because of an error, it also grabs
   the port via XvGrabPort */
static GstCaps *
gst_nvxvimagesink_get_xv_support (GstNVXvImageSink * nvxvimagesink,
    GstXContext * xcontext)
{
  gint i, event_base;
  XvAdaptorInfo *adaptors;
  gint nb_formats;
  XvImageFormatValues *formats = NULL;
  guint nb_encodings;
  XvEncodingInfo *encodings = NULL;
  gulong max_w = G_MAXINT, max_h = G_MAXINT;
  GstCaps *caps = NULL;

  g_return_val_if_fail (xcontext != NULL, NULL);

  /* First let's check that XVideo extension is available */
  if (!XQueryExtension (xcontext->disp, "XVideo", &i, &event_base, &i)) {
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, SETTINGS,
        ("Could not initialise Xv output"),
        ("XVideo extension is not available"));
    return NULL;
  }
  xcontext->xv_event_base = event_base;

  /* Then we get adaptors list */
  if (Success != XvQueryAdaptors (xcontext->disp, xcontext->root,
          &xcontext->nb_adaptors, &adaptors)) {
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, SETTINGS,
        ("Could not initialise Xv output"),
        ("Failed getting XV adaptors list"));
    return NULL;
  }

  xcontext->xv_port_id = 0;

  GST_DEBUG ("Found %u XV adaptor(s)", xcontext->nb_adaptors);

  xcontext->adaptors =
      (gchar **) g_malloc0 (xcontext->nb_adaptors * sizeof (gchar *));

  /* Now fill up our adaptor name array */
  for (i = 0; i < xcontext->nb_adaptors; i++) {
    xcontext->adaptors[i] = g_strdup (adaptors[i].name);
  }

  if (nvxvimagesink->adaptor_no >= 0 &&
      nvxvimagesink->adaptor_no < xcontext->nb_adaptors) {
    /* Find xv port from user defined adaptor */
    gst_lookup_xv_port_from_adaptor (xcontext, adaptors,
        nvxvimagesink->adaptor_no);
  }

  if (!xcontext->xv_port_id) {
    /* Now search for an adaptor that supports XvImageMask */
    for (i = 0; i < xcontext->nb_adaptors && !xcontext->xv_port_id; i++) {
      gst_lookup_xv_port_from_adaptor (xcontext, adaptors, i);
      nvxvimagesink->adaptor_no = i;
    }
  }

  XvFreeAdaptorInfo (adaptors);

  if (!xcontext->xv_port_id) {
    nvxvimagesink->adaptor_no = -1;
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, BUSY,
        ("Could not initialise Xv output"), ("No port available"));
    return NULL;
  }

  {
    int count, todo = 3;
    XvAttribute *const attr = XvQueryPortAttributes (xcontext->disp,
        xcontext->xv_port_id, &count);
    static const char shared_surface[] = "XV_USE_SHARED_SURFACE";
    static const char sync_options[] = "XV_SYNC_SHARED_SURFACE";
    static const char sync_to_vblank[] = "XV_SYNC_TO_VBLANK";

    GST_DEBUG_OBJECT (nvxvimagesink, "Checking %d Xv port attributes", count);

    nvxvimagesink->have_shared_surface = FALSE;
    nvxvimagesink->have_sync_options = FALSE;
    nvxvimagesink->have_sync_to_vblank = FALSE;

    for (i = 0; ((i < count) && todo); i++)
      if (!strcmp (attr[i].name, shared_surface)) {
        const Atom atom = XInternAtom (xcontext->disp, shared_surface, False);
        GST_LOG_OBJECT (nvxvimagesink,
            "Setting use shared surface to %d", nvxvimagesink->sync_options);

        /* Allow use of pointer to video frame */
        XvSetPortAttribute (xcontext->disp, xcontext->xv_port_id, atom, (nvxvimagesink->shared_surface ? 1 : 1));       /* TODO */
        todo--;
        nvxvimagesink->have_shared_surface = TRUE;
      } else if (!strcmp (attr[i].name, sync_options)) {
        const Atom atom = XInternAtom (xcontext->disp, sync_options, False);
        GST_LOG_OBJECT (nvxvimagesink,
            "Setting sync option to %d", nvxvimagesink->sync_options);

        XvSetPortAttribute (xcontext->disp, xcontext->xv_port_id, atom,
            nvxvimagesink->sync_options);
        todo--;
        nvxvimagesink->have_sync_options = TRUE;
      } else if (!strcmp (attr[i].name, sync_to_vblank)) {
        const Atom atom = XInternAtom (xcontext->disp, sync_to_vblank, False);
        GST_LOG_OBJECT (nvxvimagesink,
            "Setting sync to vblank %d", nvxvimagesink->sync_to_vblank);

        XvSetPortAttribute (xcontext->disp, xcontext->xv_port_id, atom,
            nvxvimagesink->sync_to_vblank);
        todo--;
        nvxvimagesink->have_sync_to_vblank = TRUE;
      }

    XFree (attr);
  }

  if (!nvxvimagesink->have_shared_surface) {
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, BUSY,
        ("Could not find suitable Xv output port"),
        ("No matching port available"));
    return NULL;
  }

  /* Get the list of encodings supported by the adapter and look for the
   * XV_IMAGE encoding so we can determine the maximum width and height
   * supported */
  XvQueryEncodings (xcontext->disp, xcontext->xv_port_id, &nb_encodings,
      &encodings);

  for (i = 0; i < nb_encodings; i++) {
    GST_LOG_OBJECT (nvxvimagesink,
        "Encoding %d, name %s, max wxh %lux%lu rate %d/%d",
        i, encodings[i].name, encodings[i].width, encodings[i].height,
        encodings[i].rate.numerator, encodings[i].rate.denominator);
    if (strcmp (encodings[i].name, "XV_IMAGE") == 0) {
      max_w = encodings[i].width;
      max_h = encodings[i].height;
    }
  }

  XvFreeEncodingInfo (encodings);

  /* We get all image formats supported by our port */
  formats = XvListImageFormats (xcontext->disp,
      xcontext->xv_port_id, &nb_formats);
  caps = gst_caps_new_empty ();
  for (i = 0; i < nb_formats; i++) {
    GstCaps *format_caps = NULL;
    GstNVXvImageFormat *format = NULL;

    if (formats[i].type != XvYUV)
      continue;

    format_caps = gst_caps_new_simple ("video/x-nvrm-yuv",
        "format", GST_TYPE_FOURCC, formats[i].id,
        "width", GST_TYPE_INT_RANGE, 1, max_w,
        "height", GST_TYPE_INT_RANGE, 1, max_h,
        "framerate", GST_TYPE_FRACTION_RANGE, 0, 1, G_MAXINT, 1, NULL);

    if (!format_caps)
      continue;

    /* We set the image format of the xcontext to an existing one. This
       is just some valid image format for making our xshm calls check before
       caps negotiation really happens. */
    xcontext->im_format = formats[i].id;

    format = g_new0 (GstNVXvImageFormat, 1);
    if (format) {
      format->format = formats[i].id;
      format->caps = gst_caps_copy (format_caps);
      xcontext->formats_list = g_list_append (xcontext->formats_list, format);
    }

    gst_caps_append (caps, format_caps);
  }

  if (formats)
    XFree (formats);

  GST_DEBUG ("Generated the following caps: %" GST_PTR_FORMAT, caps);

  if (gst_caps_is_empty (caps)) {
    gst_caps_unref (caps);
    XvUngrabPort (xcontext->disp, xcontext->xv_port_id, 0);
    GST_ELEMENT_ERROR (nvxvimagesink, STREAM, WRONG_TYPE, (NULL),
        ("No supported format found"));
    return NULL;
  }

  return caps;
}

static gpointer
gst_nvxvimagesink_event_thread (GstNVXvImageSink * nvxvimagesink)
{
  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), NULL);

  GST_OBJECT_LOCK (nvxvimagesink);
  while (nvxvimagesink->running) {
    GST_OBJECT_UNLOCK (nvxvimagesink);

    if (nvxvimagesink->xwindow) {
      gst_nvxvimagesink_handle_xevents (nvxvimagesink);
    }
    /* FIXME: do we want to align this with the framerate or anything else? */
    g_usleep (G_USEC_PER_SEC / 20);

    GST_OBJECT_LOCK (nvxvimagesink);
  }
  GST_OBJECT_UNLOCK (nvxvimagesink);

  return NULL;
}

static void
gst_nvxvimagesink_manage_event_thread (GstNVXvImageSink * nvxvimagesink)
{
  GThread *thread = NULL;

  /* don't start the thread too early */
  if (nvxvimagesink->xcontext == NULL) {
    return;
  }

  GST_OBJECT_LOCK (nvxvimagesink);
  if (!nvxvimagesink->event_thread) {
    /* Setup our event listening thread */
    GST_DEBUG_OBJECT (nvxvimagesink, "run xevent thread, expose %d, events %d",
        nvxvimagesink->handle_expose, nvxvimagesink->handle_events);
    nvxvimagesink->running = TRUE;
    nvxvimagesink->event_thread = g_thread_create (
        (GThreadFunc) gst_nvxvimagesink_event_thread, nvxvimagesink, TRUE,
        NULL);
  }
  GST_OBJECT_UNLOCK (nvxvimagesink);

  /* Wait for our event thread to finish */
  if (thread)
    g_thread_join (thread);
}


/* This function calculates the pixel aspect ratio based on the properties
 * in the xcontext structure and stores it there. */
static void
gst_nvxvimagesink_calculate_pixel_aspect_ratio (GstXContext * xcontext)
{
  static const gint par[][2] = {
    {1, 1},                     /* regular screen */
    {16, 15},                   /* PAL TV */
    {11, 10},                   /* 525 line Rec.601 video */
    {54, 59},                   /* 625 line Rec.601 video */
    {64, 45},                   /* 1280x1024 on 16:9 display */
    {5, 3},                     /* 1280x1024 on 4:3 display */
    {4, 3}                      /*  800x600 on 16:9 display */
  };
  gint i;
  gint index;
  gdouble ratio;
  gdouble delta;

#define DELTA(idx) (ABS (ratio - ((gdouble) par[idx][0] / par[idx][1])))

  /* first calculate the "real" ratio based on the X values;
   * which is the "physical" w/h divided by the w/h in pixels of the display */
  ratio = (gdouble) (xcontext->widthmm * xcontext->height)
      / (xcontext->heightmm * xcontext->width);

  /* DirectFB's X in 720x576 reports the physical dimensions wrong, so
   * override here */
  if (xcontext->width == 720 && xcontext->height == 576) {
    ratio = 4.0 * 576 / (3.0 * 720);
  }
  GST_DEBUG ("calculated pixel aspect ratio: %f", ratio);
  /* now find the one from par[][2] with the lowest delta to the real one */
  delta = DELTA (0);
  index = 0;

  for (i = 1; i < sizeof (par) / (sizeof (gint) * 2); ++i) {
    gdouble this_delta = DELTA (i);

    if (this_delta < delta) {
      index = i;
      delta = this_delta;
    }
  }

  GST_DEBUG ("Decided on index %d (%d/%d)", index,
      par[index][0], par[index][1]);

  g_free (xcontext->par);
  xcontext->par = g_new0 (GValue, 1);
  g_value_init (xcontext->par, GST_TYPE_FRACTION);
  gst_value_set_fraction (xcontext->par, par[index][0], par[index][1]);
  GST_DEBUG ("set xcontext PAR to %d/%d",
      gst_value_get_fraction_numerator (xcontext->par),
      gst_value_get_fraction_denominator (xcontext->par));
}

/* This function gets the X Display and global info about it. Everything is
   stored in our object and will be cleaned when the object is disposed. Note
   here that caps for supported format are generated without any window or
   image creation */
static GstXContext *
gst_nvxvimagesink_xcontext_get (GstNVXvImageSink * nvxvimagesink)
{
  GstXContext *xcontext = NULL;
  XPixmapFormatValues *px_formats = NULL;
  gint nb_formats = 0, i, N_attr;
  XvAttribute *xv_attr;

  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), NULL);

  xcontext = g_new0 (GstXContext, 1);
  xcontext->im_format = 0;

  g_mutex_lock (nvxvimagesink->x_lock);

  xcontext->disp = XOpenDisplay (nvxvimagesink->display_name);

  if (!xcontext->disp) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_free (xcontext);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, WRITE,
        ("Could not initialise Xv output"), ("Could not open display"));
    return NULL;
  }

  xcontext->screen = DefaultScreenOfDisplay (xcontext->disp);
  xcontext->screen_num = DefaultScreen (xcontext->disp);
  xcontext->visual = DefaultVisual (xcontext->disp, xcontext->screen_num);
  xcontext->root = DefaultRootWindow (xcontext->disp);
  xcontext->white = XWhitePixel (xcontext->disp, xcontext->screen_num);
  xcontext->black = XBlackPixel (xcontext->disp, xcontext->screen_num);
  xcontext->depth = DefaultDepthOfScreen (xcontext->screen);

  xcontext->width = DisplayWidth (xcontext->disp, xcontext->screen_num);
  xcontext->height = DisplayHeight (xcontext->disp, xcontext->screen_num);
  xcontext->widthmm = DisplayWidthMM (xcontext->disp, xcontext->screen_num);
  xcontext->heightmm = DisplayHeightMM (xcontext->disp, xcontext->screen_num);

  GST_DEBUG_OBJECT (nvxvimagesink, "X reports %dx%d pixels and %d mm x %d mm",
      xcontext->width, xcontext->height, xcontext->widthmm, xcontext->heightmm);

  gst_nvxvimagesink_calculate_pixel_aspect_ratio (xcontext);
  /* We get supported pixmap formats at supported depth */
  px_formats = XListPixmapFormats (xcontext->disp, &nb_formats);

  if (!px_formats) {
    XCloseDisplay (xcontext->disp);
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_free (xcontext->par);
    g_free (xcontext);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, SETTINGS,
        ("Could not initialise Xv output"), ("Could not get pixel formats"));
    return NULL;
  }

  /* We get bpp value corresponding to our running depth */
  for (i = 0; i < nb_formats; i++) {
    if (px_formats[i].depth == xcontext->depth)
      xcontext->bpp = px_formats[i].bits_per_pixel;
  }

  XFree (px_formats);

  xcontext->endianness =
      (ImageByteOrder (xcontext->disp) ==
      LSBFirst) ? G_LITTLE_ENDIAN : G_BIG_ENDIAN;

  /* our caps system handles 24/32bpp RGB as big-endian. */
  if ((xcontext->bpp == 24 || xcontext->bpp == 32) &&
      xcontext->endianness == G_LITTLE_ENDIAN) {
    xcontext->endianness = G_BIG_ENDIAN;
    xcontext->visual->red_mask = GUINT32_TO_BE (xcontext->visual->red_mask);
    xcontext->visual->green_mask = GUINT32_TO_BE (xcontext->visual->green_mask);
    xcontext->visual->blue_mask = GUINT32_TO_BE (xcontext->visual->blue_mask);
    if (xcontext->bpp == 24) {
      xcontext->visual->red_mask >>= 8;
      xcontext->visual->green_mask >>= 8;
      xcontext->visual->blue_mask >>= 8;
    }
  }

  xcontext->caps = gst_nvxvimagesink_get_xv_support (nvxvimagesink, xcontext);

  if (!xcontext->caps) {
    XCloseDisplay (xcontext->disp);
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_free (xcontext->par);
    g_free (xcontext);
    /* GST_ELEMENT_ERROR is thrown by gst_nvxvimagesink_get_xv_support */
    return NULL;
  }

  if (!XShmQueryExtension (xcontext->disp) ||
      !gst_xvimagesink_check_xshm_calls (xcontext)) {
    XCloseDisplay (xcontext->disp);
    g_mutex_unlock (nvxvimagesink->x_lock);
    g_free (xcontext->par);
    g_free (xcontext);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, SETTINGS,
        ("Could not initialise Xv output"),
        ("XShm extension is not available"));
    return NULL;
  }

  xv_attr = XvQueryPortAttributes (xcontext->disp,
      xcontext->xv_port_id, &N_attr);

  if (xv_attr)
    XFree (xv_attr);

  g_mutex_unlock (nvxvimagesink->x_lock);

  return xcontext;
}

/* This function cleans the X context. Closing the Display, releasing the XV
   port and unrefing the caps for supported formats. */
static void
gst_nvxvimagesink_xcontext_clear (GstNVXvImageSink * nvxvimagesink)
{
  GList *formats_list;
  GstXContext *xcontext;
  gint i = 0;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  GST_OBJECT_LOCK (nvxvimagesink);
  if (nvxvimagesink->xcontext == NULL) {
    GST_OBJECT_UNLOCK (nvxvimagesink);
    return;
  }

  /* Take the XContext from the sink and clean it up */
  xcontext = nvxvimagesink->xcontext;
  nvxvimagesink->xcontext = NULL;

  GST_OBJECT_UNLOCK (nvxvimagesink);


  formats_list = xcontext->formats_list;

  while (formats_list) {
    GstNVXvImageFormat *format = formats_list->data;

    gst_caps_unref (format->caps);
    g_free (format);
    formats_list = g_list_next (formats_list);
  }

  if (xcontext->formats_list)
    g_list_free (xcontext->formats_list);

  gst_caps_unref (xcontext->caps);

  for (i = 0; i < xcontext->nb_adaptors; i++) {
    g_free (xcontext->adaptors[i]);
  }

  g_free (xcontext->adaptors);

  g_free (xcontext->par);

  g_mutex_lock (nvxvimagesink->x_lock);

  GST_DEBUG_OBJECT (nvxvimagesink, "Closing display and freeing X Context");

  XvUngrabPort (xcontext->disp, xcontext->xv_port_id, 0);

  XCloseDisplay (xcontext->disp);

  g_mutex_unlock (nvxvimagesink->x_lock);

  g_free (xcontext);
}

/* Element stuff */

/* This function tries to get a format matching with a given caps in the
   supported list of formats we generated in gst_nvxvimagesink_get_xv_support */
static gint
gst_nvxvimagesink_get_format_from_caps (GstNVXvImageSink * nvxvimagesink,
    GstCaps * caps)
{
  GList *list = NULL;

  g_return_val_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink), 0);

  list = nvxvimagesink->xcontext->formats_list;

  while (list) {
    GstNVXvImageFormat *format = list->data;

    if (format) {
      if (gst_caps_can_intersect (caps, format->caps)) {
        return format->format;
      }
    }
    list = g_list_next (list);
  }

  return -1;
}

static GstCaps *
gst_nvxvimagesink_getcaps (GstBaseSink * bsink)
{
  GstNVXvImageSink *nvxvimagesink;

  nvxvimagesink = GST_NVXVIMAGESINK (bsink);

  if (nvxvimagesink->xcontext)
    return gst_caps_ref (nvxvimagesink->xcontext->caps);

  return
      gst_caps_copy (gst_pad_get_pad_template_caps (GST_VIDEO_SINK_PAD
          (nvxvimagesink)));
}

static gboolean
gst_nvxvimagesink_setcaps (GstBaseSink * bsink, GstCaps * caps)
{
  GstNVXvImageSink *nvxvimagesink;
  GstStructure *structure;
  guint32 im_format = 0;
  gboolean ret;
  gboolean fullscreen = FALSE;
  gint video_width, video_height;
  gint disp_x, disp_y;
  gint disp_width, disp_height;
  gint video_par_n, video_par_d;        /* video's PAR */
  gint display_par_n, display_par_d;    /* display's PAR */
  const GValue *caps_par;
  const GValue *caps_disp_reg;
  const GValue *fps;
  guint num, den;
  int (*handler) (Display *, XErrorEvent *);
  int todo = 0;                 /* TODO support multiple images */

  nvxvimagesink = GST_NVXVIMAGESINK (bsink);

  GST_DEBUG_OBJECT (nvxvimagesink,
      "In setcaps. Possible caps %" GST_PTR_FORMAT ", setting caps %"
      GST_PTR_FORMAT, nvxvimagesink->xcontext->caps, caps);

  /* get nvmap fd */
  if (!gst_nvxvimagesink_get_nvmap_fd (nvxvimagesink)) {
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, NOT_FOUND,
        ("Couldn't get nvmap/knvmap fd"), (NULL));
    return FALSE;
  }

  if (!gst_caps_can_intersect (nvxvimagesink->xcontext->caps, caps))
    goto incompatible_caps;

  structure = gst_caps_get_structure (caps, 0);
  ret = gst_structure_get_int (structure, "width", &video_width);
  ret &= gst_structure_get_int (structure, "height", &video_height);
  fps = gst_structure_get_value (structure, "framerate");
  ret &= (fps != NULL);

  if (!ret)
    goto incomplete_caps;

  nvxvimagesink->fps_n = gst_value_get_fraction_numerator (fps);
  nvxvimagesink->fps_d = gst_value_get_fraction_denominator (fps);

  im_format = gst_nvxvimagesink_get_format_from_caps (nvxvimagesink, caps);
  if (im_format == -1)
    goto invalid_format;

  nvxvimagesink->buf_width = video_width;
  nvxvimagesink->buf_height = video_height;
  nvxvimagesink->im_format = im_format;
  switch (im_format) {
    case GST_MAKE_FOURCC ('I', '4', '2', '0'):
      break;
    default:
      goto invalid_format;
  }

  if (G_UNLIKELY (nvxvimagesink->xvimages[todo].xvimage)) {
    GST_DEBUG_OBJECT (nvxvimagesink, "already have image");
    gst_nvxvimagesink_image_destroy (nvxvimagesink);
  }

  g_mutex_lock (nvxvimagesink->x_lock);

  error_caught = FALSE;
  XSync (nvxvimagesink->xcontext->disp, FALSE);
  handler = XSetErrorHandler (gst_nvxvimagesink_handle_xerror);
  /* TODO don't alloc image of data size, we are going to send
     buffer descriptor and not the actual data */
  nvxvimagesink->xvimages[todo].xvimage =
      XvShmCreateImage (nvxvimagesink->xcontext->disp,
      nvxvimagesink->xcontext->xv_port_id, nvxvimagesink->im_format, 0,
      nvxvimagesink->buf_width, nvxvimagesink->buf_height,
      &nvxvimagesink->xvimages[todo].SHMInfo);
  XSync (nvxvimagesink->xcontext->disp, FALSE);
  if (!nvxvimagesink->xvimages[todo].xvimage || error_caught) {
    error_caught = FALSE;
    XSetErrorHandler (handler);
    g_mutex_unlock (nvxvimagesink->x_lock);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, WRITE,
        ("Failed to create output XvImage"), ("XvCreateImage failed"));
    goto no_xvimage;
  }
  error_caught = FALSE;
  XSetErrorHandler (handler);

  nvxvimagesink->xvimages[todo].SHMInfo.shmid =
      shmget (IPC_PRIVATE, nvxvimagesink->xvimages[todo].xvimage->data_size,
      IPC_CREAT | 0777);
  if (nvxvimagesink->xvimages[todo].SHMInfo.shmid == -1) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, WRITE,
        ("Failed to create output image buffer of %dx%d pixels",
            nvxvimagesink->buf_width, nvxvimagesink->buf_height),
        ("could not get shared memory of %" G_GSIZE_FORMAT " bytes",
            nvxvimagesink->xvimages[todo].xvimage->data_size));
    goto no_attach;
  }

  nvxvimagesink->xvimages[todo].SHMInfo.shmaddr =
      shmat (nvxvimagesink->xvimages[todo].SHMInfo.shmid, 0, 0);
  if (nvxvimagesink->xvimages[todo].SHMInfo.shmaddr == ((void *) -1)) {
    g_mutex_unlock (nvxvimagesink->x_lock);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, WRITE,
        ("Failed to create output image buffer of %dx%d pixels",
            nvxvimagesink->buf_width, nvxvimagesink->buf_height),
        ("Failed to shmat: %s", g_strerror (errno)));
    /* Clean up the shared memory segment */
    shmctl (nvxvimagesink->xvimages[todo].SHMInfo.shmid, IPC_RMID, NULL);
    goto no_attach;
  }

  nvxvimagesink->xvimages[todo].xvimage->data =
      nvxvimagesink->xvimages[todo].SHMInfo.shmaddr;
  nvxvimagesink->xvimages[todo].SHMInfo.readOnly = FALSE;

  if (XShmAttach (nvxvimagesink->xcontext->disp,
          &nvxvimagesink->xvimages[todo].SHMInfo) == 0) {
    /* Clean up the shared memory segment */
    shmctl (nvxvimagesink->xvimages[todo].SHMInfo.shmid, IPC_RMID, NULL);
    g_mutex_unlock (nvxvimagesink->x_lock);
    GST_ELEMENT_ERROR (nvxvimagesink, RESOURCE, WRITE,
        ("Failed to create output image buffer of %dx%d pixels",
            nvxvimagesink->buf_width, nvxvimagesink->buf_height),
        ("Failed to XShmAttach"));
    goto no_attach;
  }

  XSync (nvxvimagesink->xcontext->disp, FALSE);

  /* Delete the shared memory segment as soon as we everyone is attached.
   * This way, it will be deleted as soon as we detach later, and not
   * leaked if we crash. */
  shmctl (nvxvimagesink->xvimages[todo].SHMInfo.shmid, IPC_RMID, NULL);

  g_mutex_unlock (nvxvimagesink->x_lock);

  GST_DEBUG_OBJECT (nvxvimagesink, "XServer ShmAttached to 0x%x, id 0x%lx",
      nvxvimagesink->xvimages[todo].SHMInfo.shmid,
      nvxvimagesink->xvimages[todo].SHMInfo.shmseg);

  /* get aspect ratio from caps if it's present, and
   * convert video width and height to a display width and height
   * using wd / hd = wv / hv * PARv / PARd */

  /* get video's PAR */
  caps_par = gst_structure_get_value (structure, "pixel-aspect-ratio");
  if (caps_par) {
    video_par_n = gst_value_get_fraction_numerator (caps_par);
    video_par_d = gst_value_get_fraction_denominator (caps_par);
  } else {
    video_par_n = 1;
    video_par_d = 1;
  }
  /* get display's PAR */
  if (nvxvimagesink->par) {
    display_par_n = gst_value_get_fraction_numerator (nvxvimagesink->par);
    display_par_d = gst_value_get_fraction_denominator (nvxvimagesink->par);
  } else {
    display_par_n = 1;
    display_par_d = 1;
  }

  /* get the display region */
  caps_disp_reg = gst_structure_get_value (structure, "display-region");
  if (caps_disp_reg) {
    disp_x = g_value_get_int (gst_value_array_get_value (caps_disp_reg, 0));
    disp_y = g_value_get_int (gst_value_array_get_value (caps_disp_reg, 1));
    disp_width = g_value_get_int (gst_value_array_get_value (caps_disp_reg, 2));
    disp_height =
        g_value_get_int (gst_value_array_get_value (caps_disp_reg, 3));
  } else {
    disp_x = disp_y = 0;
    disp_width = video_width;
    disp_height = video_height;
  }

  if (!gst_video_calculate_display_ratio (&num, &den, video_width,
          video_height, video_par_n, video_par_d, display_par_n, display_par_d))
    goto no_disp_ratio;

  nvxvimagesink->disp_x = disp_x;
  nvxvimagesink->disp_y = disp_y;
  nvxvimagesink->disp_width = disp_width;
  nvxvimagesink->disp_height = disp_height;

  GST_DEBUG_OBJECT (nvxvimagesink,
      "video width/height: %dx%d, calculated display ratio: %d/%d",
      video_width, video_height, num, den);

  /* FIXME need to check if clipping to screen dimension is required ? */
  if ((video_width > nvxvimagesink->xcontext->width
          && video_height > nvxvimagesink->xcontext->height)
      || nvxvimagesink->fullscreen_window) {
    video_width = nvxvimagesink->xcontext->width;
    video_height = nvxvimagesink->xcontext->height;
    fullscreen = TRUE;
  } else {
    if (video_width > nvxvimagesink->xcontext->width)
      video_width = nvxvimagesink->xcontext->width;
    if (video_height > nvxvimagesink->xcontext->height)
      video_height = nvxvimagesink->xcontext->height;
  }

  /* now find a width x height that respects this display ratio.
   * prefer those that have one of w/h the same as the incoming video
   * using wd / hd = num / den */

  /* start with same height, because of interlaced video */
  /* check hd / den is an integer scale factor, and scale wd with the PAR */
  if (video_height % den == 0) {
    GST_DEBUG_OBJECT (nvxvimagesink, "keeping video height");
    GST_VIDEO_SINK_WIDTH (nvxvimagesink) = (guint)
        gst_util_uint64_scale_int (video_height, num, den);
    GST_VIDEO_SINK_HEIGHT (nvxvimagesink) = video_height;
  } else if (video_width % num == 0) {
    GST_DEBUG_OBJECT (nvxvimagesink, "keeping video width");
    GST_VIDEO_SINK_WIDTH (nvxvimagesink) = video_width;
    GST_VIDEO_SINK_HEIGHT (nvxvimagesink) = (guint)
        gst_util_uint64_scale_int (video_width, den, num);
  } else {
    GST_DEBUG_OBJECT (nvxvimagesink,
        "approximating while keeping video height");
    GST_VIDEO_SINK_WIDTH (nvxvimagesink) = (guint)
        gst_util_uint64_scale_int (video_height, num, den);
    GST_VIDEO_SINK_HEIGHT (nvxvimagesink) = video_height;
  }

  GST_DEBUG_OBJECT (nvxvimagesink, "scaling to %dx%d",
      GST_VIDEO_SINK_WIDTH (nvxvimagesink),
      GST_VIDEO_SINK_HEIGHT (nvxvimagesink));

  /* Notify application to set xwindow id now */
  g_mutex_lock (nvxvimagesink->flow_lock);
  if (!nvxvimagesink->xwindow) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    gst_x_overlay_prepare_xwindow_id (GST_X_OVERLAY (nvxvimagesink));
  } else {
    g_mutex_unlock (nvxvimagesink->flow_lock);
  }

  /* Creating our window and our image with the display size in pixels */
  if (GST_VIDEO_SINK_WIDTH (nvxvimagesink) <= 0 ||
      GST_VIDEO_SINK_HEIGHT (nvxvimagesink) <= 0)
    goto no_display_size;

  g_mutex_lock (nvxvimagesink->flow_lock);
  if (!nvxvimagesink->xwindow) {
    if (fullscreen) {
      nvxvimagesink->xwindow = gst_nvxvimagesink_xwindow_new (nvxvimagesink,
          nvxvimagesink->xcontext->width, nvxvimagesink->xcontext->height);
    } else {
      nvxvimagesink->xwindow = gst_nvxvimagesink_xwindow_new (nvxvimagesink,
          GST_VIDEO_SINK_WIDTH (nvxvimagesink),
          GST_VIDEO_SINK_HEIGHT (nvxvimagesink));
    }
  }

  /* After a resize, we want to redraw the borders in case the new frame size
   * doesn't cover the same area */
  nvxvimagesink->redraw_border = TRUE;

  g_mutex_unlock (nvxvimagesink->flow_lock);

  return TRUE;

  /* ERRORS */
no_attach:
  {
    gst_nvxvimagesink_image_destroy (nvxvimagesink);
    GST_ERROR_OBJECT (nvxvimagesink, "XShmAttach failed");
    return FALSE;
  }
no_xvimage:
  {
    GST_ERROR_OBJECT (nvxvimagesink, "XvCreateImage failed");
    return FALSE;
  }
incompatible_caps:
  {
    GST_ERROR_OBJECT (nvxvimagesink, "caps incompatible");
    return FALSE;
  }
incomplete_caps:
  {
    GST_DEBUG_OBJECT (nvxvimagesink, "Failed to retrieve either width, "
        "height or framerate from intersected caps");
    return FALSE;
  }
invalid_format:
  {
    GST_DEBUG_OBJECT (nvxvimagesink,
        "Could not locate image format from caps %" GST_PTR_FORMAT, caps);
    return FALSE;
  }
no_disp_ratio:
  {
    GST_ELEMENT_ERROR (nvxvimagesink, CORE, NEGOTIATION, (NULL),
        ("Error calculating the output display ratio of the video."));
    return FALSE;
  }
no_display_size:
  {
    GST_ELEMENT_ERROR (nvxvimagesink, CORE, NEGOTIATION, (NULL),
        ("Error calculating the output display ratio of the video."));
    return FALSE;
  }
}

static gboolean
gst_nvxvimagesink_check_fd_valid (int fd)
{
  if (fd < 3 || fd >= FD_SETSIZE)
    return FALSE;
  if (fcntl (fd, F_GETFL) == -1 && errno == EBADF)
    return FALSE;
  return TRUE;
}

static gboolean
gst_nvxvimagesink_get_nvmap_fd (GstNVXvImageSink * nvxvimagesink)
{
  GDir *dir;
  gchar *pid_path;
  const gchar *fd_name = NULL;
  gboolean found_fd = FALSE;

  pid_path = g_strdup_printf ("/proc/%lu/fd", (gulong) getpid ());
  g_return_val_if_fail (pid_path != NULL, FALSE);

  if ((dir = g_dir_open (pid_path, 0, NULL)) == NULL) {
    g_free (pid_path);
    return FALSE;
  }

  while (!found_fd && (fd_name = g_dir_read_name (dir)) != NULL) {
    int fd;
    gchar *fd_path;
    gchar *fd_resolved_path;
    gchar *file_name;

    fd_path = g_strdup_printf ("%s/%s", pid_path, fd_name);
    fd_resolved_path = g_file_read_link (fd_path, NULL);
    g_free (fd_path);
    if (fd_resolved_path == NULL)
      continue;

    file_name = g_path_get_basename (fd_resolved_path);
    if ((strcmp (file_name, "knvmap") == 0) ||
        (strcmp (file_name, "nvmap") == 0)) {
      fd = atoi (fd_name);
      found_fd = gst_nvxvimagesink_check_fd_valid (fd);
      if (found_fd)
        nvxvimagesink->nvmap_fd = fd;
    }

    g_free (file_name);
    g_free (fd_resolved_path);
  }

  g_free (pid_path);
  g_dir_close (dir);

  return found_fd;
}

static GstStateChangeReturn
gst_nvxvimagesink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstNVXvImageSink *nvxvimagesink;
  GstXContext *xcontext = NULL;

  nvxvimagesink = GST_NVXVIMAGESINK (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      /* Initializing the XContext */
      if (nvxvimagesink->xcontext == NULL) {
        xcontext = gst_nvxvimagesink_xcontext_get (nvxvimagesink);
        if (xcontext == NULL)
          return GST_STATE_CHANGE_FAILURE;
        GST_OBJECT_LOCK (nvxvimagesink);
        if (xcontext)
          nvxvimagesink->xcontext = xcontext;
        GST_OBJECT_UNLOCK (nvxvimagesink);
      }

      /* update object's par with calculated one if not set yet */
      if (!nvxvimagesink->par) {
        nvxvimagesink->par = g_new0 (GValue, 1);
        gst_value_init_and_copy (nvxvimagesink->par,
            nvxvimagesink->xcontext->par);
        GST_DEBUG_OBJECT (nvxvimagesink, "set calculated PAR on object's PAR");
      }
      /* call XSynchronize with the current value of synchronous */
      GST_DEBUG_OBJECT (nvxvimagesink, "XSynchronize called with %s",
          nvxvimagesink->synchronous ? "TRUE" : "FALSE");
      XSynchronize (nvxvimagesink->xcontext->disp, nvxvimagesink->synchronous);
      gst_nvxvimagesink_manage_event_thread (nvxvimagesink);
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      nvxvimagesink->fps_n = 0;
      nvxvimagesink->fps_d = 1;
      GST_VIDEO_SINK_WIDTH (nvxvimagesink) = 0;
      GST_VIDEO_SINK_HEIGHT (nvxvimagesink) = 0;
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      gst_nvxvimagesink_reset (nvxvimagesink);
      break;
    default:
      break;
  }

  return ret;
}

static void
gst_nvxvimagesink_get_times (GstBaseSink * bsink, GstBuffer * buf,
    GstClockTime * start, GstClockTime * end)
{
  GstNVXvImageSink *nvxvimagesink;

  nvxvimagesink = GST_NVXVIMAGESINK (bsink);

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buf)) {
    *start = GST_BUFFER_TIMESTAMP (buf);
    if (GST_BUFFER_DURATION_IS_VALID (buf)) {
      *end = *start + GST_BUFFER_DURATION (buf);
    } else {
      if (nvxvimagesink->fps_n > 0) {
        *end = *start +
            gst_util_uint64_scale_int (GST_SECOND, nvxvimagesink->fps_d,
            nvxvimagesink->fps_n);
      }
    }
  }
}

static GstFlowReturn
gst_nvxvimagesink_show_frame (GstVideoSink * vsink, GstBuffer * buf_gst)
{
  GstNVXvImageSink *nvxvimagesink;
  GstNVXvImageBuffer *buf_nv;   /* newly-wrapped buffer */

  nvxvimagesink = GST_NVXVIMAGESINK (vsink);

  buf_nv = gst_nvxvimagesink_buffer_wrap (nvxvimagesink, buf_gst);
  if (!buf_nv)
    goto no_wrap;

  if (!gst_nvxvimagesink_nvxvimage_put (nvxvimagesink, buf_nv)) {
    gst_nvxvimagesink_buffer_free (buf_nv);
    goto no_window;
  }

  /* TODO for now lets free immediately */
  gst_nvxvimagesink_buffer_free (buf_nv);

  return GST_FLOW_OK;

  /* ERRORS */
no_window:
  {
    /* No Window available to put our image into */
    GST_WARNING_OBJECT (nvxvimagesink, "could not output image - no window");
    return GST_FLOW_ERROR;
  }
no_wrap:
  {
    /* Couldn't wrap the incoming buffer in a NV object */
    GST_WARNING_OBJECT (nvxvimagesink, "could not wrap buffer");
    return GST_FLOW_ERROR;
  }
}

static gboolean
gst_nvxvimagesink_event (GstBaseSink * sink, GstEvent * event)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (sink);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_TAG:{
      GstTagList *l;
      gchar *title = NULL;

      gst_event_parse_tag (event, &l);
      gst_tag_list_get_string (l, GST_TAG_TITLE, &title);

      if (title) {
        GST_DEBUG_OBJECT (nvxvimagesink, "got tags, title='%s'", title);
        gst_nvxvimagesink_xwindow_set_title (nvxvimagesink,
            nvxvimagesink->xwindow, title);

        g_free (title);
      }
      break;
    }
#if 0
    case GST_EVENT_CROP:{
      gst_event_parse_crop (event,
          &nvxvimagesink->video_y_offset,
          &nvxvimagesink->video_x_offset, NULL, NULL);

      return TRUE;
    }
#endif
    default:
      break;
  }
  if (GST_BASE_SINK_CLASS (parent_class)->event)
    return GST_BASE_SINK_CLASS (parent_class)->event (sink, event);
  else
    return TRUE;
}

/* Interfaces stuff */

static gboolean
gst_nvxvimagesink_interface_supported (GstImplementsInterface * iface,
    GType type)
{
  g_assert (type == GST_TYPE_NAVIGATION || type == GST_TYPE_X_OVERLAY ||
      type == GST_TYPE_PROPERTY_PROBE);
  return TRUE;
}

static void
gst_nvxvimagesink_interface_init (GstImplementsInterfaceClass * klass)
{
  klass->supported = gst_nvxvimagesink_interface_supported;
}

static void
gst_nvxvimagesink_navigation_send_event (GstNavigation * navigation,
    GstStructure * structure)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (navigation);
  GstPad *peer;

  if ((peer = gst_pad_get_peer (GST_VIDEO_SINK_PAD (nvxvimagesink)))) {
    GstEvent *event;
    GstVideoRectangle src, dst, result;
    gdouble x, y, xscale = 1.0, yscale = 1.0;

    event = gst_event_new_navigation (structure);

    /* We take the flow_lock while we look at the window */
    g_mutex_lock (nvxvimagesink->flow_lock);

    if (!nvxvimagesink->xwindow) {
      g_mutex_unlock (nvxvimagesink->flow_lock);
      return;
    }

    if (nvxvimagesink->keep_aspect) {
      /* We get the frame position using the calculated geometry from _setcaps
         that respect pixel aspect ratios */
      src.w = GST_VIDEO_SINK_WIDTH (nvxvimagesink);
      src.h = GST_VIDEO_SINK_HEIGHT (nvxvimagesink);
      dst.w = nvxvimagesink->render_rect.w;
      dst.h = nvxvimagesink->render_rect.h;

      gst_video_sink_center_rect (src, dst, &result, TRUE);
      result.x += nvxvimagesink->render_rect.x;
      result.y += nvxvimagesink->render_rect.y;
    } else {
      memcpy (&result, &nvxvimagesink->render_rect, sizeof (GstVideoRectangle));
    }

    g_mutex_unlock (nvxvimagesink->flow_lock);

    /* We calculate scaling using the original video frames geometry to include
       pixel aspect ratio scaling. */
    xscale = (gdouble) nvxvimagesink->video_width / result.w;
    yscale = (gdouble) nvxvimagesink->video_height / result.h;

    /* Converting pointer coordinates to the non scaled geometry */
    if (gst_structure_get_double (structure, "pointer_x", &x)) {
      x = MIN (x, result.x + result.w);
      x = MAX (x - result.x, 0);
      gst_structure_set (structure, "pointer_x", G_TYPE_DOUBLE,
          (gdouble) x * xscale, NULL);
    }
    if (gst_structure_get_double (structure, "pointer_y", &y)) {
      y = MIN (y, result.y + result.h);
      y = MAX (y - result.y, 0);
      gst_structure_set (structure, "pointer_y", G_TYPE_DOUBLE,
          (gdouble) y * yscale, NULL);
    }

    gst_pad_send_event (peer, event);
    gst_object_unref (peer);
  }
}

static void
gst_nvxvimagesink_navigation_init (GstNavigationInterface * iface)
{
  iface->send_event = gst_nvxvimagesink_navigation_send_event;
}

static void
gst_nvxvimagesink_set_window_handle (GstXOverlay * overlay, gulong id)
{
  XID xwindow_id = id;
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (overlay);
  GstXWindow *xwindow = NULL;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (nvxvimagesink));

  g_mutex_lock (nvxvimagesink->flow_lock);

  /* If we already use that window return */
  if (nvxvimagesink->xwindow && (xwindow_id == nvxvimagesink->xwindow->win)) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    return;
  }

  /* If the element has not initialized the X11 context try to do so */
  if (!nvxvimagesink->xcontext &&
      !(nvxvimagesink->xcontext =
          gst_nvxvimagesink_xcontext_get (nvxvimagesink))) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    /* we have thrown a GST_ELEMENT_ERROR now */
    return;
  }

  gst_nvxvimagesink_image_destroy (nvxvimagesink);

  /* If a window is there already we destroy it */
  if (nvxvimagesink->xwindow) {
    gst_nvxvimagesink_xwindow_destroy (nvxvimagesink, nvxvimagesink->xwindow);
    nvxvimagesink->xwindow = NULL;
  }

  /* If the xid is 0 we go back to an internal window */
  if (xwindow_id == 0) {
    /* If no width/height caps nego did not happen window will be created
       during caps nego then */
    if (GST_VIDEO_SINK_WIDTH (nvxvimagesink)
        && GST_VIDEO_SINK_HEIGHT (nvxvimagesink)) {
      xwindow =
          gst_nvxvimagesink_xwindow_new (nvxvimagesink,
          GST_VIDEO_SINK_WIDTH (nvxvimagesink),
          GST_VIDEO_SINK_HEIGHT (nvxvimagesink));
    }
  } else {
    XWindowAttributes attr;

    xwindow = g_new0 (GstXWindow, 1);
    xwindow->win = xwindow_id;

    /* Set the event we want to receive and create a GC */
    g_mutex_lock (nvxvimagesink->x_lock);

    XGetWindowAttributes (nvxvimagesink->xcontext->disp, xwindow->win, &attr);

    xwindow->width = attr.width;
    xwindow->height = attr.height;
    xwindow->internal = FALSE;
    if (!nvxvimagesink->have_render_rect) {
      nvxvimagesink->render_rect.x = nvxvimagesink->render_rect.y = 0;
      nvxvimagesink->render_rect.w = attr.width;
      nvxvimagesink->render_rect.h = attr.height;
    }
    if (nvxvimagesink->handle_events) {
      XSelectInput (nvxvimagesink->xcontext->disp, xwindow->win, ExposureMask |
          StructureNotifyMask | PointerMotionMask | KeyPressMask |
          KeyReleaseMask);
    }

    xwindow->gc = XCreateGC (nvxvimagesink->xcontext->disp,
        xwindow->win, 0, NULL);
    g_mutex_unlock (nvxvimagesink->x_lock);
  }

  if (xwindow)
    nvxvimagesink->xwindow = xwindow;

  g_mutex_unlock (nvxvimagesink->flow_lock);
}

static void
gst_nvxvimagesink_expose (GstXOverlay * overlay)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (overlay);

  gst_nvxvimagesink_xwindow_update_geometry (nvxvimagesink);
  gst_nvxvimagesink_nvxvimage_put (nvxvimagesink, NULL);
}

static void
gst_nvxvimagesink_set_event_handling (GstXOverlay * overlay,
    gboolean handle_events)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (overlay);

  nvxvimagesink->handle_events = handle_events;

  g_mutex_lock (nvxvimagesink->flow_lock);

  if (G_UNLIKELY (!nvxvimagesink->xwindow)) {
    g_mutex_unlock (nvxvimagesink->flow_lock);
    return;
  }

  g_mutex_lock (nvxvimagesink->x_lock);

  if (handle_events) {
    if (nvxvimagesink->xwindow->internal) {
      XSelectInput (nvxvimagesink->xcontext->disp,
          nvxvimagesink->xwindow->win,
          ExposureMask | StructureNotifyMask | PointerMotionMask | KeyPressMask
          | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask);
    } else {
      XSelectInput (nvxvimagesink->xcontext->disp,
          nvxvimagesink->xwindow->win,
          ExposureMask | StructureNotifyMask | PointerMotionMask | KeyPressMask
          | KeyReleaseMask);
    }
  } else {
    XSelectInput (nvxvimagesink->xcontext->disp, nvxvimagesink->xwindow->win,
        0);
  }

  g_mutex_unlock (nvxvimagesink->x_lock);

  g_mutex_unlock (nvxvimagesink->flow_lock);
}

#if 0
static void
gst_nvxvimagesink_set_render_rectangle (GstXOverlay * overlay, gint x, gint y,
    gint width, gint height)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (overlay);

  /* FIXME: how about some locking? */
  if (width >= 0 && height >= 0) {
    nvxvimagesink->render_rect.x = x;
    nvxvimagesink->render_rect.y = y;
    nvxvimagesink->render_rect.w = width;
    nvxvimagesink->render_rect.h = height;
    nvxvimagesink->have_render_rect = TRUE;
  } else {
    nvxvimagesink->render_rect.x = 0;
    nvxvimagesink->render_rect.y = 0;
    nvxvimagesink->render_rect.w = nvxvimagesink->xwindow->width;
    nvxvimagesink->render_rect.h = nvxvimagesink->xwindow->height;
    nvxvimagesink->have_render_rect = FALSE;
  }
}
#endif

static void
gst_nvxvimagesink_xoverlay_init (GstXOverlayClass * iface)
{
  iface->set_xwindow_id = gst_nvxvimagesink_set_window_handle;
  iface->expose = gst_nvxvimagesink_expose;
  iface->handle_events = gst_nvxvimagesink_set_event_handling;
#if 0
  iface->set_render_rectangle = gst_nvxvimagesink_set_render_rectangle;
#endif
}

static const GList *
gst_nvxvimagesink_probe_get_properties (GstPropertyProbe * probe)
{
  GObjectClass *klass = G_OBJECT_GET_CLASS (probe);
  static GList *list = NULL;

  if (!list) {
    list = g_list_append (NULL, g_object_class_find_property (klass, "device"));
    list =
        g_list_append (list, g_object_class_find_property (klass,
            "shared-surface"));
    list =
        g_list_append (list, g_object_class_find_property (klass,
            "sync-options"));

    list =
        g_list_append (list, g_object_class_find_property (klass,
            "sync-to-vblank"));
  }

  return list;
}

static void
gst_nvxvimagesink_probe_probe_property (GstPropertyProbe * probe,
    guint prop_id, const GParamSpec * pspec)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (probe);

  switch (prop_id) {
    case PROP_DEVICE:
    case PROP_SHARED_SURFACE:
    case PROP_SYNC_OPTIONS:
    case PROP_SYNC_TO_VBLANK:
      GST_DEBUG_OBJECT (nvxvimagesink,
          "probing device list and get capabilities");
      if (!nvxvimagesink->xcontext) {
        GST_DEBUG_OBJECT (nvxvimagesink, "generating xcontext");
        nvxvimagesink->xcontext =
            gst_nvxvimagesink_xcontext_get (nvxvimagesink);
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (probe, prop_id, pspec);
      break;
  }
}

static gboolean
gst_nvxvimagesink_probe_needs_probe (GstPropertyProbe * probe,
    guint prop_id, const GParamSpec * pspec)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (probe);
  gboolean ret = FALSE;

  switch (prop_id) {
    case PROP_DEVICE:
    case PROP_SHARED_SURFACE:
    case PROP_SYNC_OPTIONS:
    case PROP_SYNC_TO_VBLANK:
      if (nvxvimagesink->xcontext != NULL) {
        ret = FALSE;
      } else {
        ret = TRUE;
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (probe, prop_id, pspec);
      break;
  }

  return ret;
}

static GValueArray *
gst_nvxvimagesink_probe_get_values (GstPropertyProbe * probe,
    guint prop_id, const GParamSpec * pspec)
{
  GstNVXvImageSink *nvxvimagesink = GST_NVXVIMAGESINK (probe);
  GValueArray *array = NULL;

  if (G_UNLIKELY (!nvxvimagesink->xcontext)) {
    GST_WARNING_OBJECT (nvxvimagesink, "we don't have any xcontext, can't "
        "get values");
    goto beach;
  }

  switch (prop_id) {
    case PROP_DEVICE:
    {
      guint i;
      GValue value = { 0 };

      array = g_value_array_new (nvxvimagesink->xcontext->nb_adaptors);
      g_value_init (&value, G_TYPE_STRING);

      for (i = 0; i < nvxvimagesink->xcontext->nb_adaptors; i++) {
        gchar *adaptor_id_s = g_strdup_printf ("%u", i);

        g_value_set_string (&value, adaptor_id_s);
        g_value_array_append (array, &value);
        g_free (adaptor_id_s);
      }
      g_value_unset (&value);
      break;
    }
    case PROP_SHARED_SURFACE:
      if (nvxvimagesink->have_shared_surface) {
        GValue value = { 0 };

        array = g_value_array_new (2);
        g_value_init (&value, G_TYPE_BOOLEAN);
        g_value_set_boolean (&value, FALSE);
        g_value_array_append (array, &value);
        g_value_set_boolean (&value, TRUE);
        g_value_array_append (array, &value);
        g_value_unset (&value);
      }
      break;
    case PROP_SYNC_OPTIONS:
      if (nvxvimagesink->sync_options) {
        GValue value = { 0 };

        array = g_value_array_new (1);
        g_value_init (&value, GST_TYPE_INT_RANGE);
        gst_value_set_int_range (&value, 0, 0xffffff);
        g_value_array_append (array, &value);
        g_value_unset (&value);
      }
      break;
    case PROP_SYNC_TO_VBLANK:
      if (nvxvimagesink->sync_to_vblank) {
        GValue value = { 0 };

        array = g_value_array_new (2);
        g_value_init (&value, G_TYPE_BOOLEAN);
        g_value_set_boolean (&value, FALSE);
        g_value_array_append (array, &value);
        g_value_set_boolean (&value, TRUE);
        g_value_array_append (array, &value);
        g_value_unset (&value);
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (probe, prop_id, pspec);
      break;
  }

beach:
  return array;
}

static void
gst_nvxvimagesink_property_probe_interface_init (GstPropertyProbeInterface *
    iface)
{
  iface->get_properties = gst_nvxvimagesink_probe_get_properties;
  iface->probe_property = gst_nvxvimagesink_probe_probe_property;
  iface->needs_probe = gst_nvxvimagesink_probe_needs_probe;
  iface->get_values = gst_nvxvimagesink_probe_get_values;
}

/* =========================================== */
/*                                             */
/*              Init & Class init              */
/*                                             */
/* =========================================== */

static void
gst_nvxvimagesink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstNVXvImageSink *nvxvimagesink;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (object));

  nvxvimagesink = GST_NVXVIMAGESINK (object);

  switch (prop_id) {
    case PROP_DISPLAY:
      nvxvimagesink->display_name = g_strdup (g_value_get_string (value));
      break;
    case PROP_SYNCHRONOUS:
      nvxvimagesink->synchronous = g_value_get_boolean (value);
      if (nvxvimagesink->xcontext) {
        XSynchronize (nvxvimagesink->xcontext->disp,
            nvxvimagesink->synchronous);
        GST_DEBUG_OBJECT (nvxvimagesink, "XSynchronize called with %s",
            nvxvimagesink->synchronous ? "TRUE" : "FALSE");
      }
      break;
    case PROP_PIXEL_ASPECT_RATIO:
      g_free (nvxvimagesink->par);
      nvxvimagesink->par = g_new0 (GValue, 1);
      g_value_init (nvxvimagesink->par, GST_TYPE_FRACTION);
      if (!g_value_transform (value, nvxvimagesink->par)) {
        g_warning ("Could not transform string to aspect ratio");
        gst_value_set_fraction (nvxvimagesink->par, 1, 1);
      }
      GST_DEBUG_OBJECT (nvxvimagesink, "set PAR to %d/%d",
          gst_value_get_fraction_numerator (nvxvimagesink->par),
          gst_value_get_fraction_denominator (nvxvimagesink->par));
      break;
    case PROP_FORCE_ASPECT_RATIO:
      nvxvimagesink->keep_aspect = g_value_get_boolean (value);
      break;
    case PROP_FORCE_FULLSCREEN:
      nvxvimagesink->fullscreen_window = g_value_get_boolean (value);
      break;
    case PROP_HANDLE_EVENTS:
      gst_nvxvimagesink_set_event_handling (GST_X_OVERLAY (nvxvimagesink),
          g_value_get_boolean (value));
      gst_nvxvimagesink_manage_event_thread (nvxvimagesink);
      break;
    case PROP_DEVICE:
      nvxvimagesink->adaptor_no = atoi (g_value_get_string (value));
      break;
    case PROP_HANDLE_EXPOSE:
      nvxvimagesink->handle_expose = g_value_get_boolean (value);
      gst_nvxvimagesink_manage_event_thread (nvxvimagesink);
      break;
    case PROP_SHARED_SURFACE:
      nvxvimagesink->shared_surface = g_value_get_boolean (value);
      break;
    case PROP_SYNC_OPTIONS:
      nvxvimagesink->sync_options = g_value_get_int (value);
      break;
    case PROP_SYNC_TO_VBLANK:
      nvxvimagesink->sync_to_vblank = g_value_get_boolean (value);
      break;
    case PROP_DRAW_BORDERS:
      nvxvimagesink->draw_borders = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_nvxvimagesink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstNVXvImageSink *nvxvimagesink;

  g_return_if_fail (GST_IS_NVXVIMAGESINK (object));

  nvxvimagesink = GST_NVXVIMAGESINK (object);

  switch (prop_id) {
    case PROP_DISPLAY:
      g_value_set_string (value, nvxvimagesink->display_name);
      break;
    case PROP_SYNCHRONOUS:
      g_value_set_boolean (value, nvxvimagesink->synchronous);
      break;
    case PROP_PIXEL_ASPECT_RATIO:
      if (nvxvimagesink->par)
        g_value_transform (nvxvimagesink->par, value);
      break;
    case PROP_FORCE_ASPECT_RATIO:
      g_value_set_boolean (value, nvxvimagesink->keep_aspect);
      break;
    case PROP_FORCE_FULLSCREEN:
      g_value_set_boolean (value, nvxvimagesink->fullscreen_window);
      break;
    case PROP_HANDLE_EVENTS:
      g_value_set_boolean (value, nvxvimagesink->handle_events);
      break;
    case PROP_DEVICE:
    {
      char *adaptor_no_s = g_strdup_printf ("%u", nvxvimagesink->adaptor_no);

      g_value_set_string (value, adaptor_no_s);
      g_free (adaptor_no_s);
      break;
    }
    case PROP_DEVICE_NAME:
      if (nvxvimagesink->xcontext && nvxvimagesink->xcontext->adaptors) {
        g_value_set_string (value,
            nvxvimagesink->xcontext->adaptors[nvxvimagesink->adaptor_no]);
      } else {
        g_value_set_string (value, NULL);
      }
      break;
    case PROP_HANDLE_EXPOSE:
      g_value_set_boolean (value, nvxvimagesink->handle_expose);
      break;
    case PROP_SHARED_SURFACE:
      g_value_set_boolean (value, nvxvimagesink->shared_surface);
      break;
    case PROP_SYNC_OPTIONS:
      g_value_set_int (value, nvxvimagesink->sync_options);
      break;
    case PROP_SYNC_TO_VBLANK:
      g_value_set_boolean (value, nvxvimagesink->sync_to_vblank);
      break;
    case PROP_DRAW_BORDERS:
      g_value_set_boolean (value, nvxvimagesink->draw_borders);
      break;
    case PROP_WINDOW_WIDTH:
      if (nvxvimagesink->xwindow)
        g_value_set_uint64 (value, nvxvimagesink->xwindow->width);
      else
        g_value_set_uint64 (value, 0);
      break;
    case PROP_WINDOW_HEIGHT:
      if (nvxvimagesink->xwindow)
        g_value_set_uint64 (value, nvxvimagesink->xwindow->height);
      else
        g_value_set_uint64 (value, 0);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_nvxvimagesink_reset (GstNVXvImageSink * nvxvimagesink)
{
  GThread *thread;

  GST_OBJECT_LOCK (nvxvimagesink);
  nvxvimagesink->running = FALSE;
  /* grab thread and mark it as NULL */
  thread = nvxvimagesink->event_thread;
  nvxvimagesink->event_thread = NULL;
  GST_OBJECT_UNLOCK (nvxvimagesink);

  /* Wait for our event thread to finish before we clean up our stuff. */
  if (thread)
    g_thread_join (thread);

  gst_nvxvimagesink_image_destroy (nvxvimagesink);

  if (nvxvimagesink->xwindow) {
    gst_nvxvimagesink_xwindow_clear (nvxvimagesink, nvxvimagesink->xwindow);
    gst_nvxvimagesink_xwindow_destroy (nvxvimagesink, nvxvimagesink->xwindow);
    nvxvimagesink->xwindow = NULL;
  }

  nvxvimagesink->render_rect.x = 0;
  nvxvimagesink->render_rect.y = 0;
  nvxvimagesink->render_rect.w = 0;
  nvxvimagesink->render_rect.h = 0;
  nvxvimagesink->have_render_rect = FALSE;

  gst_nvxvimagesink_xcontext_clear (nvxvimagesink);
}

/* Finalize is called only once, dispose can be called multiple times.
 * We use mutexes and don't reset stuff to NULL here so let's register
 * as a finalize. */
static void
gst_nvxvimagesink_finalize (GObject * object)
{
  GstNVXvImageSink *nvxvimagesink;

  nvxvimagesink = GST_NVXVIMAGESINK (object);

  gst_nvxvimagesink_reset (nvxvimagesink);

  if (nvxvimagesink->display_name) {
    g_free (nvxvimagesink->display_name);
    nvxvimagesink->display_name = NULL;
  }

  if (nvxvimagesink->par) {
    g_free (nvxvimagesink->par);
    nvxvimagesink->par = NULL;
  }
  if (nvxvimagesink->x_lock) {
    g_mutex_free (nvxvimagesink->x_lock);
    nvxvimagesink->x_lock = NULL;
  }
  if (nvxvimagesink->flow_lock) {
    g_mutex_free (nvxvimagesink->flow_lock);
    nvxvimagesink->flow_lock = NULL;
  }

  g_free (nvxvimagesink->media_title);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_nvxvimagesink_init (GstNVXvImageSink * nvxvimagesink)
{
  nvxvimagesink->display_name = NULL;
  nvxvimagesink->adaptor_no = 0;
  nvxvimagesink->xcontext = NULL;
  nvxvimagesink->xwindow = NULL;

  nvxvimagesink->fps_n = 0;
  nvxvimagesink->fps_d = 0;
  nvxvimagesink->video_x_offset = 0;
  nvxvimagesink->video_y_offset = 0;
  nvxvimagesink->video_width = 0;
  nvxvimagesink->video_height = 0;
  nvxvimagesink->buf_width = 0;
  nvxvimagesink->buf_height = 0;

  nvxvimagesink->x_lock = g_mutex_new ();
  nvxvimagesink->flow_lock = g_mutex_new ();

  nvxvimagesink->synchronous = FALSE;
  nvxvimagesink->running = FALSE;
  nvxvimagesink->keep_aspect = TRUE;
  nvxvimagesink->fullscreen_window = FALSE;
  nvxvimagesink->handle_events = TRUE;
  nvxvimagesink->par = NULL;
  nvxvimagesink->handle_expose = TRUE;
  nvxvimagesink->shared_surface = TRUE;
  nvxvimagesink->sync_options = 0;
  nvxvimagesink->sync_to_vblank = TRUE;
  nvxvimagesink->draw_borders = TRUE;
}

static void
gst_nvxvimagesink_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details_simple (element_class,
      "Video sink", "Sink/Video",
      "A modified Xv based videosink", "Yogish Kulkarni <yogishk@nvidia.com>");

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_nvxvimagesink_sink_template_factory));
}

static void
gst_nvxvimagesink_class_init (GstNVXvImageSinkClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSinkClass *gstbasesink_class;
  GstVideoSinkClass *videosink_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  gstbasesink_class = (GstBaseSinkClass *) klass;
  videosink_class = (GstVideoSinkClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->set_property = gst_nvxvimagesink_set_property;
  gobject_class->get_property = gst_nvxvimagesink_get_property;

  g_object_class_install_property (gobject_class, PROP_DISPLAY,
      g_param_spec_string ("display", "Display", "X Display name", NULL,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_SYNCHRONOUS,
      g_param_spec_boolean ("synchronous", "Synchronous",
          "When enabled, runs "
          "the X display in synchronous mode. (used only for debugging)", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_PIXEL_ASPECT_RATIO,
      g_param_spec_string ("pixel-aspect-ratio", "Pixel Aspect Ratio",
          "The pixel aspect ratio of the device", "1/1",
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_FORCE_ASPECT_RATIO,
      g_param_spec_boolean ("force-aspect-ratio", "Force aspect ratio",
          "When enabled, scaling will respect original aspect ratio", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_HANDLE_EVENTS,
      g_param_spec_boolean ("handle-events", "Handle XEvents",
          "When enabled, XEvents will be selected and handled", TRUE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DEVICE,
      g_param_spec_string ("device", "Adaptor number",
          "The number of the video adaptor", "0",
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_DEVICE_NAME,
      g_param_spec_string ("device-name", "Adaptor name",
          "The name of the video adaptor", NULL,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (gobject_class, PROP_FORCE_FULLSCREEN,
      g_param_spec_boolean ("force-fullscreen", "Force fullscreen window",
          "When enabled, fullscreen window will be created", FALSE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  /**
   * GstNVXvImageSink:handle-expose
   *
   * When enabled, the current frame will always be drawn in response to X
   * Expose.
   *
   * Since: 0.10.14
   */
  g_object_class_install_property (gobject_class, PROP_HANDLE_EXPOSE,
      g_param_spec_boolean ("handle-expose", "Handle expose",
          "When enabled, "
          "the current frame will always be drawn in response to X Expose "
          "events", TRUE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstNVXvImageSink:shared-surface
   *
   * Whether to pass pointer to video frame
   *
   */
  g_object_class_install_property (gobject_class, PROP_SHARED_SURFACE,
      g_param_spec_boolean ("shared-surface", "Use shared surface",
          "Whether to pass pointer to video frame", TRUE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstNVXvImageSink:sync-options
   *
   * Whether to sync hw operations on shared surface immediate,
   * deferred or nosync.
   *
   */
  g_object_class_install_property (gobject_class, PROP_SYNC_OPTIONS,
      g_param_spec_int ("sync-options", "sync options",
          "Sync hw operations on shared surface immediate, deferred or nosync",
          G_MININT, G_MAXINT, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

   /**
   * GstNVXvImageSink:sync-to-vblank
   *
   * Whether to sync video rendering to vblank
   *
   */
  g_object_class_install_property (gobject_class, PROP_SYNC_TO_VBLANK,
      g_param_spec_boolean ("sync-to-vblank", "sync to vblank",
          "Whether to sync video rendering to vblank", TRUE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstNVXvImageSink:draw-borders
   *
   * Draw black borders when using GstNVXvImageSink:force-aspect-ratio to fill
   * unused parts of the video area.
   *
   * Since: 0.10.21
   */
  g_object_class_install_property (gobject_class, PROP_DRAW_BORDERS,
      g_param_spec_boolean ("draw-borders", "Colorkey",
          "Draw black borders to fill unused area in force-aspect-ratio mode",
          TRUE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  /**
   * GstNVXvImageSink:window-width
   *
   * Actual width of the video window.
   *
   * Since: 0.10.32
   */
  g_object_class_install_property (gobject_class, PROP_WINDOW_WIDTH,
      g_param_spec_uint64 ("window-width", "window-width",
          "Width of the window", 0, G_MAXUINT64, 0,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * GstNVXvImageSink:window-height
   *
   * Actual height of the video window.
   *
   * Since: 0.10.32
   */
  g_object_class_install_property (gobject_class, PROP_WINDOW_HEIGHT,
      g_param_spec_uint64 ("window-height", "window-height",
          "Height of the window", 0, G_MAXUINT64, 0,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  gobject_class->finalize = gst_nvxvimagesink_finalize;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_nvxvimagesink_change_state);

  gstbasesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_nvxvimagesink_getcaps);
  gstbasesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_nvxvimagesink_setcaps);
  gstbasesink_class->get_times =
      GST_DEBUG_FUNCPTR (gst_nvxvimagesink_get_times);
  gstbasesink_class->event = GST_DEBUG_FUNCPTR (gst_nvxvimagesink_event);

  videosink_class->show_frame =
      GST_DEBUG_FUNCPTR (gst_nvxvimagesink_show_frame);
}

/* ============================================================= */
/*                                                               */
/*                       Public Methods                          */
/*                                                               */
/* ============================================================= */

/* =========================================== */
/*                                             */
/*          Object typing & Creation           */
/*                                             */
/* =========================================== */

static GType
gst_nvxvimage_buffer_get_type (void)
{
  static GType _gst_nvxvimage_buffer_type;

  if (G_UNLIKELY (_gst_nvxvimage_buffer_type == 0)) {
    static const GTypeInfo nvxvimage_buffer_info = {
      sizeof (GstMiniObjectClass),
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      sizeof (GstNVXvImageBuffer),
      0,
      NULL,
      NULL
    };
    _gst_nvxvimage_buffer_type = g_type_register_static (GST_TYPE_MINI_OBJECT,
        "GstNVXvImageBuffer", &nvxvimage_buffer_info, 0);
  }
  return _gst_nvxvimage_buffer_type;
}

GType
gst_nvxvimagesink_get_type (void)
{
  static GType nvxvimagesink_type = 0;

  if (!nvxvimagesink_type) {
    static const GTypeInfo nvxvimagesink_info = {
      sizeof (GstNVXvImageSinkClass),
      gst_nvxvimagesink_base_init,
      NULL,
      (GClassInitFunc) gst_nvxvimagesink_class_init,
      NULL,
      NULL,
      sizeof (GstNVXvImageSink),
      0,
      (GInstanceInitFunc) gst_nvxvimagesink_init,
    };
    static const GInterfaceInfo iface_info = {
      (GInterfaceInitFunc) gst_nvxvimagesink_interface_init,
      NULL,
      NULL,
    };
    static const GInterfaceInfo navigation_info = {
      (GInterfaceInitFunc) gst_nvxvimagesink_navigation_init,
      NULL,
      NULL,
    };
    static const GInterfaceInfo overlay_info = {
      (GInterfaceInitFunc) gst_nvxvimagesink_xoverlay_init,
      NULL,
      NULL,
    };
    static const GInterfaceInfo propertyprobe_info = {
      (GInterfaceInitFunc) gst_nvxvimagesink_property_probe_interface_init,
      NULL,
      NULL,
    };
    nvxvimagesink_type = g_type_register_static (GST_TYPE_VIDEO_SINK,
        "GstNVXvImageSink", &nvxvimagesink_info, 0);

    g_type_add_interface_static (nvxvimagesink_type,
        GST_TYPE_IMPLEMENTS_INTERFACE, &iface_info);
    g_type_add_interface_static (nvxvimagesink_type, GST_TYPE_NAVIGATION,
        &navigation_info);
    g_type_add_interface_static (nvxvimagesink_type, GST_TYPE_X_OVERLAY,
        &overlay_info);
    g_type_add_interface_static (nvxvimagesink_type, GST_TYPE_PROPERTY_PROBE,
        &propertyprobe_info);


    /* register type and create class in a more safe place instead of at
     * runtime since the type registration and class creation is not
     * threadsafe. */
    g_type_class_ref (gst_nvxvimage_buffer_get_type ());
  }

  return nvxvimagesink_type;
}

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "nvxvimagesink",
          GST_RANK_PRIMARY, GST_TYPE_NVXVIMAGESINK))
    return FALSE;

  GST_DEBUG_CATEGORY_INIT (gst_debug_nvxvimagesink, "nvxvimagesink", 0,
      "nvxvimagesink element");
  GST_DEBUG_CATEGORY_GET (GST_CAT_PERFORMANCE, "GST_PERFORMANCE");

  return TRUE;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "nvxvimagesink",
    "Video sink using Nvidia custom buffers and modified X video",
    plugin_init, VERSION, GST_LICENSE, GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN)

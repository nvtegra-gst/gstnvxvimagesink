/* GStreamer
 * Copyright (C) <2005> Julien Moutte <julien@moutte.net>
 *               <2011> Texas Instruments, Inc. <daniels@collabora.com>
 * Copyright (c) 2012, NVIDIA CORPORATION.  All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_NVXVIMAGESINK_H__
#define __GST_NVXVIMAGESINK_H__

#include <gst/video/gstvideosink.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>

#include <X11/extensions/Xv.h>
#include <X11/extensions/Xvlib.h>

#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>

G_BEGIN_DECLS
#define GST_TYPE_NVXVIMAGESINK \
  (gst_nvxvimagesink_get_type())
#define GST_NVXVIMAGESINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_NVXVIMAGESINK, GstNVXvImageSink))
#define GST_NVXVIMAGESINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_NVXVIMAGESINK, GstNVXvImageSinkClass))
#define GST_IS_NVXVIMAGESINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_NVXVIMAGESINK))
#define GST_IS_NVXVIMAGESINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_NVXVIMAGESINK))
typedef struct _GstXContext GstXContext;
typedef struct _GstXWindow GstXWindow;
typedef struct _GstNVXvImageFormat GstNVXvImageFormat;
typedef struct _GstNVXvImageBuffer GstNVXvImageBuffer;
typedef struct _GstXvImageBuffer GstXvImageBuffer;

typedef struct _GstNVXvImageSink GstNVXvImageSink;
typedef struct _GstNVXvImageSinkClass GstNVXvImageSinkClass;

#define NUM_XVIMAGES  (1)

/*
 * GstXContext:
 * @disp: the X11 Display of this context
 * @screen: the default Screen of Display @disp
 * @screen_num: the Screen number of @screen
 * @visual: the default Visual of Screen @screen
 * @root: the root Window of Display @disp
 * @white: the value of a white pixel on Screen @screen
 * @black: the value of a black pixel on Screen @screen
 * @depth: the color depth of Display @disp
 * @bpp: the number of bits per pixel on Display @disp
 * @endianness: the endianness of image bytes on Display @disp
 * @width: the width in pixels of Display @disp
 * @height: the height in pixels of Display @disp
 * @widthmm: the width in millimeters of Display @disp
 * @heightmm: the height in millimeters of Display @disp
 * @par: the pixel aspect ratio calculated from @width, @widthmm and @height,
 * @heightmm ratio
 * @use_xshm: used to known wether of not XShm extension is usable or not even
 * if the Extension is present
 * @xv_port_id: the XVideo port ID
 * @formats_list: list of supported image formats on @xv_port_id
 * @caps: the #GstCaps that Display @disp can accept
 *
 * Structure used to store various informations collected/calculated for a
 * Display.
 */
struct _GstXContext
{
  Display *disp;

  Screen *screen;
  gint screen_num;

  Visual *visual;

  Window root;

  gulong white, black;

  gint depth;
  gint bpp;
  gint endianness;

  gint width, height;
  gint widthmm, heightmm;
  GValue *par;                  /* calculated pixel aspect ratio */

  XvPortID xv_port_id;
  guint nb_adaptors;
  gchar **adaptors;
  gint im_format;

  GList *formats_list;

  GstCaps *caps;

  gint xv_event_base;
};

/*
 * GstXWindow:
 * @win: the Window ID of this X11 window
 * @width: the width in pixels of Window @win
 * @height: the height in pixels of Window @win
 * @internal: used to remember if Window @win was created internally or passed
 * through the #GstXOverlay interface
 * @gc: the Graphical Context of Window @win
 *
 * Structure used to store informations about a Window.
 */
struct _GstXWindow
{
  Window win;
  gint width, height;
  gboolean internal;
  GC gc;
};

/**
 * GstNVXvImageFormat:
 * @format: the image format
 * @caps: generated #GstCaps for this image format
 *
 * Structure storing image format to #GstCaps association.
 */
struct _GstNVXvImageFormat
{
  uint32_t format;
  GstCaps *caps;
};

/**
 * GstXImageBuffer:
 * @xvimagesink: a reference to our #GstXvImageSink
 * @xvimage: the XvImage of this buffer
 * @width: the width in pixels of XvImage @xvimage
 * @height: the height in pixels of XvImage @xvimage
 * @im_format: the image format of XvImage @xvimage
 * @size: the size in bytes of XvImage @xvimage
 *
 * Subclass of #GstBuffer containing additional information about an XvImage.
 */
struct _GstXvImageBuffer
{
  GstBuffer buffer;

  /* Reference to the xvimagesink we belong to */
  GstNVXvImageSink *nvxvimagesink;

  XvImage *xvimage;

  XShmSegmentInfo SHMInfo;

  gint width, height, im_format;
  size_t size;
};


/**
 * GstNVXvImageBuffer:
 * @nvxvimagesink: a reference to our #GstNVXvImageSink
 */
struct _GstNVXvImageBuffer
{
  GstMiniObject obj;
  /* Reference to the GstBuffer we're wrapping */
  GstBuffer *buf_gst;
  GstNVXvImageSink *nvxvimagesink;
  uint32_t id;
};

/**
 * GstNVXvImageSink:
 * @display_name: the name of the Display we want to render to
 * @xcontext: our instance's #GstXContext
 * @xwindow: the #GstXWindow we are rendering to
 * @xvimage: internal #GstXvImage used to store incoming buffers and render when
 * not using the buffer_alloc optimization mechanism
 * @cur_image: a reference to the last #GstXvImage that was put to @xwindow. It
 * is used when Expose events are received to redraw the latest video frame
 * @event_thread: a thread listening for events on @xwindow and handling them
 * @running: used to inform @event_thread if it should run/shutdown
 * @fps_n: the framerate fraction numerator
 * @fps_d: the framerate fraction denominator
 * @x_lock: used to protect X calls as we are not using the XLib in threaded
 * mode
 * @flow_lock: used to protect data flow routines from external calls such as
 * events from @event_thread or methods from the #GstXOverlay interface
 * @par: used to override calculated pixel aspect ratio from @xcontext
 * @pool_lock: used to protect the buffer pool
 * @image_pool: a list of #GstXvImageBuffer that could be reused at next buffer
 * allocation call
 * @synchronous: used to store if XSynchronous should be used or not (for
 * debugging purpose only)
 * @keep_aspect: used to remember if reverse negotiation scaling should respect
 * aspect ratio
 * @handle_events: used to know if we should handle select XEvents or not
 * @video_width: the width of incoming video frames in pixels
 * @video_height: the height of incoming video frames in pixels
 *
 * The #GstNVXvImageSink data structure.
 */
struct _GstNVXvImageSink
{
  /* Our element stuff */
  GstVideoSink videosink;
  uint32_t nvmap_fd;

  char *display_name;
  guint adaptor_no;

  GstXContext *xcontext;
  GstXWindow *xwindow;

  GThread *event_thread;
  gboolean running;

  gint fps_n;
  gint fps_d;

  GMutex *x_lock;
  GMutex *flow_lock;

  /* object-set pixel aspect ratio */
  GValue *par;

  gboolean synchronous;
  gboolean keep_aspect;
  gboolean fullscreen_window;
  gboolean redraw_border;
  gboolean handle_events;
  gboolean handle_expose;

  /* width and height of video */
  guint video_width, video_height;
  /* offset, in pixels, into the buffer */
  guint video_x_offset, video_y_offset;
  /* total buffer width, height */
  guint buf_width, buf_height;
  uint32_t buf_format;
  gint im_format;

  GstXvImageBuffer xvimages[NUM_XVIMAGES];

  /* display sizes, used for clipping the image */
  gint disp_x, disp_y;
  gint disp_width, disp_height;

  /* port attributes */
  gboolean shared_surface;
  gint sync_options;
  gint sync_to_vblank;

  gboolean draw_borders;

  /* port features */
  gboolean have_shared_surface;
  gint have_sync_options;
  gint have_sync_to_vblank;

  /* stream metadata */
  gchar *media_title;

  /* target video rectagle */
  GstVideoRectangle render_rect;
  gboolean have_render_rect;
};

struct _GstNVXvImageSinkClass
{
  GstVideoSinkClass parent_class;
};

GType gst_nvxvimagesink_get_type (void);

G_END_DECLS
#endif /* __GST_NVXVIMAGESINK_H__ */

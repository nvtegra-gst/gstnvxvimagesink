/*
Copyright (c) 2012, NVIDIA CORPORATION.  All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
*/

#ifndef INCLUDED_TEGRA_VIDEO_H
#define INCLUDED_TEGRA_VIDEO_H

#define TEGRAXVSURFACEDESCRIPTOR_MAX_SURFACES 3

typedef struct tegraRMSurfaceStruct {
    unsigned int Width;
    unsigned int Height;
    unsigned int ColorFormat;
    unsigned int Layout;
    unsigned int Pitch;
    unsigned int hMem;
    unsigned int Offset;
    unsigned int pBase;
} tegraRMSurface;

typedef struct tegraXVSurfaceDescriptorStruct {
    unsigned int size;
    unsigned int surfCount;
    unsigned int memIds[TEGRAXVSURFACEDESCRIPTOR_MAX_SURFACES];
    tegraRMSurface rmsurfs[TEGRAXVSURFACEDESCRIPTOR_MAX_SURFACES];
} tegraXVSurfaceDescriptor;

enum TEGRATransformOptions {
    TEGRATransformOption_None = 0,
    TEGRATransformOption_Rotate90,
    TEGRATransformOption_Rotate180,
    TEGRATransformOption_Rotate270,
    TEGRATransformOption_FlipHorizontal,
    TEGRATransformOption_InvTranspose,
    TEGRATransformOption_FlipVertical,
    TEGRATransformOption_Transpose,
    TEGRATransformOption_Invalid
};

#endif
